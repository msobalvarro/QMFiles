<?php
require '../fpdf/fpdf.php';

@$data = $_GET['data'];

if($data = base64_decode($data)){

    require "../php/connect.php";

    $consulta = "CALL `generate_data_fact`('$data')";

    $respuesta = $mysql->query($consulta);




    @session_start();
    #Variables y Sesiones
    $business = $_SESSION['nombre_empresa'];
    $logo = $_SESSION['logo_empresa'];
    $datos_temp = $row = $respuesta->fetch_assoc();
    #Configurar Página PDF
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetTitle('Factura de Venta');
    $pdf->SetFont('Arial','',16);
    #***********************************

    #Encabezado (Nombre Y Logo)
    $pdf->Cell(190,20,$business, 1,1,'C');
    $pdf->Image('../logos/logoqm.png',15,15,20,0);
    #***********************************

    #Información de Factura
    $pdf->SetFont('Arial','',12);
    $pdf->Cell(140, 10, 'Fecha: '.$datos_temp['fecha'], 0,'LR');
    $pdf->SetTextColor(230, 10, 10);
    $pdf->Cell(50, 10, 'N# '.$datos_temp['num_fact'], 0, 'LR');
    $pdf->Ln();
    $pdf->SetTextColor(10, 10, 10);

    $pdf->Cell(140, 20, 'Cliente:__________________________________________', 0, 'LR');

    $pdf->Cell(25, 10, 'Credito', 0);
    $pdf->Rect(155, 50, 5, 5, '');

    $pdf->Cell(25, 10, 'Contado', 0);
    $pdf->Rect(180, 50, 5, 5, '');
    $pdf->Ln(20);

    $pdf->Cell(20, 8, 'Cant', 1, 'LR', 'C');
    $pdf->Cell(40, 8, 'Codigo', 1, 'LR', 'L');
    $pdf->Cell(110, 8, 'Nombre', 1, 'LR', 'L');
    $pdf->Cell(20, 8, 'Monto', 1, 1, 'C');

    $pdf->SetFont('Arial','',10);
    $sub_total = 0;
    while($row = $respuesta->fetch_assoc()){
        $total_monto = number_format($row['product_price'] * $row['cantidad']);
        $sub_total += number_format($total_monto);
        $pdf->Cell(20, 6, $row['cantidad'], 1, 'LR', 'C');
        $pdf->Cell(40, 6, $row['product_code'], 1, 'LR', 'L');
        $pdf->Cell(110, 6, $row['product_name'], 1, 'LR', 'L');
        $pdf->Cell(20, 6, $total_monto, 1, 1, 'C');
        if($row['iva'] == 1)
        {
            $exist_iva = true;
        }
        else
        {
            $exist_iva = false;
        }
    }
    if($exist_iva)
    {
        $pdf->Cell(170, 6, 'Sub Total', 0, 'LR', 'R');
        $pdf->Cell(20, 6, $sub_total, 1, 1, 'C');

        $pdf->Cell(170, 6, 'IVA', 0, 'LR', 'R');
        $pdf->Cell(20, 6, '15%', 1, 1, 'C');

        $pdf->Cell(170, 6, 'Total', 0, 'LR', 'R');
        $pdf->Cell(20, 6, number_format($sub_total + ($sub_total * 0.15)), 1, 1, 'C');
    }
    else
    {
        $pdf->Cell(170, 6, 'Total', 0, 'LR', 'R');
        $pdf->Cell(20, 6, $sub_total, 1, 1, 'C');
    }

    $pdf->Output();
    Close();


    session_destroy();

}
else{
    echo 'factura en construccion';
}
