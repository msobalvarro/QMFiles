<div class="center-absoluted">
    <form accept-charset="UTF-8" method="post" action="json/login.php" role="form">
       <h2 class="center-aling" style="color: #c62828 ">Usuario o clave incorrecta</h2>
        <fieldset>
            <div class="form-group">
                <input required class="form-control" placeholder="Usuario" name="usuario" type="text" autofocus>
            </div>
            <div class="form-group">
                <input required class="form-control" placeholder="Clave" name="clave" type="password" value="">
            </div>

            <div class="center">
                <input class="btn waves-effect waves-light btn-large" type="submit" value="Login">
            </div>
        </fieldset>
    </form>
</div>
