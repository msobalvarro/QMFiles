<?php

require "php/paginas.php";

if($orden_ == 1)
{
    ?>

        <div class="container" ng-init="titulo='Orden de trabajo'; traer_ot()">
            <div class="progress" ng-hide="ot">
                <div class="indeterminate"></div>
            </div>

            <ul id="tabs-swipe-demo" class="tabs">
                <li class="tab col s3">
                    <a href="#" ng-click=" filtro_tab = '' ; mostrar_grafica_x=false;" class="active waves-effect">Todo</a>
                </li>
                <li class="tab col s3">
                    <a href="#" ng-click=" filtro_tab = 'Mayor' ; mostrar_grafica_x=false;" 
                    class="waves-effect">Mantenimiento Mayor</a>
                </li>
                <li class="tab col s3">
                    <a href="#" ng-click=" filtro_tab = 'Menor'; mostrar_grafica_x=false; " class="waves-effect">Mantenimiento Menor</a>
                </li>
                <li class="tab col s3">
                    <a href="#" ng-click=" filtro_tab = 'Reacondicionamiento'; mostrar_grafica_x=false; " 
                    class="waves-effect">Reacondicionamiento</a>
                </li>
                <li class="tab col s3">
                    <a href="#" ng-click=" filtro_tab = 'Diagnóstico' ; mostrar_grafica_x=false;" 
                    class="waves-effect">Diagnóstico</a>
                </li>
                <li class="tab col s3">
                    <a href="#" ng-click="grafica_ot(); mostrar_grafica_x = true;" 
                    class="waves-effect">Comportamientos</a>
                </li>
            </ul>
            <div ng-show="mostrar_grafica_x">
                <div class="col s5" ng-init="grafica_ot()">
                    <div zingchart id="chart-1" zc-json="myJson" zc-width="100%"></div>         
                </div>
                
                <div class="divider"></div>

                <div class="row section">
                    <h5 class="teal-text center mayuscula">Filtar datos</h5>
                    <div class="col s6 input-field">
                        <input type="date" id="date_begin" 
                        placeholder="Fecha de Inicio" ng-model="date_begin">
                        <label for="date_begind" class="active">Desde</label>
                    </div>
                    <div class="col s6 input-field">
                        <input type="date" id="date_end" placeholder="Fecha de Final" ng-model="date_end">
                        <label for="date_end" class="active">Hasta</label>
                    </div>
                    <div class="center">
                        <button class="btn teal darken-3 waves-effect" ng-click="filtar_grafica_ot()">Filtar</button>
                    </div>
                </div>
            </div>
            <div id="test-swipe-2" ng-hide="mostrar_grafica_x" class="col s12 blue">
                <ul class="collection" ng-show="ot">
                    <li class="collection-item avatar lighten-3" ng-repeat="data in ot | filter:filtrador | filter: filtro_tab" 
                    ng-class="{'grey grey-text text-darken-5': !data.abierta}">
                       <i class="circle teal  center centrado_vert" 
                        ng-class="{'grey': !data.abierta, 'teal darken-3': data.abierta}" 
                        style="margin-top: 10px; font-style: normal">
                            {{ data.marca_vehiculo | limitTo:1 | uppercase }}{{ data.modelo_vehiculo | limitTo:1 | uppercase }}
                        </i>
                        <span class="title mayuscula">
                            {{ data.marca_vehiculo }} {{ data.modelo_vehiculo }} - {{ data.tipo_servicio }}
                        </span>                        
                        <p ng-class="{'grey-text text-darken-5': !data.abierta}">
                            <span>
                                Numero de orden: #{{ data.codigp_orden=  data.id_ot + data.id_vehiculo + data.id_cliente}}
                            </span>
                            <br>
                            <span class="teal-text" ng-hide="!data.abierta">
                                Costo Orden: {{ data.costo | currency:'C$':00 }}
                            </span>
                            <span ng-show="!data.abierta">
                                Fecha de cierre: {{ data.fecha_cierre }}
                            </span>
                        </p>
                        
                        <div class="fixed-action-btn horizontal secondary-content">
                            <a class="btn-floating btn-flat btn-large waves-effect">
                                <i class="large material-icons grey-text">more_vert</i>
                            </a>
                            <ul>
                                <li>
                                    <a class="btn-floating btn grey darken-2 tooltipped waves-effect" data-position="bottom" 
                                    data-delay="50" data-tooltip="Resumen" ng-click="ot_especifica(data.id_ot)">
                                        <i class="material-icons">visibility</i>
                                    </a>
                                </li>
                                <li ng-hide="!data.abierta">
                                    <a class="btn-floating btn blue-grey tooltipped waves-effect" data-position="bottom" 
                                    data-delay="50" data-tooltip="Cerrar Orden" ng-click="cerrar_orden(data.id_ot)">
                                        <i class="material-icons">lock</i>
                                    </a>
                                </li> 
                                <li ng-show="data.abierta">
                                    <a class="btn-floating btn amber darken-2 tooltipped waves-effect" data-position="bottom" 
                                    data-delay="50" data-tooltip="Modificar Orden" ng-click="">
                                        <i class="material-icons">edit</i>
                                    </a>
                                </li>
                                <?php 

                                     if($_SESSION['admin'] == 1)
                                     {
                                        ?>
                                        <li ng-show="!data.abierta">
                                            <a class="btn-floating btn brown darken-3 tooltipped waves-effect" data-position="bottom" 
                                            data-delay="50" data-tooltip="Abrir Orden" ng-click="abrir_orden(data.id_ot)">
                                                <i class="material-icons">lock_open</i>
                                            </a>
                                        </li>                            
                                        <li>
                                            <a class="btn-floating btn red lighten-1 tooltipped waves-effect" data-position="bottom" 
                                            data-delay="50" data-tooltip="Cerrar Orden" ng-click="eliminar_orden(data.id_ot)">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </li>
                                        <?php
                                     }

                                ?>
                                
                            </ul>
                        </div>
                    </li>
                </ul>                
            </div>
        </div>
        
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large waves-effect waves-light btn teal darken-3 tooltipped" 
                data-position="left" data-delay="50" data-tooltip="Agregar Orden" 
                ng-click="modal('modal1')">
                <li class="material-icons centrado_vert">add</li>
            </a>
        </div>
        
        <!-- Modal Selección vehiculo -->
        <div id="modal1" class="modal modal-fixed-footer">
            <div class="modal-content">             
                
                <!-- Preloader -->
                
                <div class="preloader-wrapper small active" ng-hide="vehiculos_ot" id="loader">
                    <div class="spinner-layer spinner-green-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="gap-patch">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">                    
                    <div class="col s8">
                        <h4>
                            <blockquote class="red-text text-lighten-1 mayuscula">
                                Seleccione Vehiculo
                            </blockquote>
                        </h4>
                    </div>
                    
                    <div class="input-field col s4">
                        <input id="search" class="mayuscula" type="search" required placeholder="Filtrar" ng-model="filtro_ot">
                        <label class="label-icon" for="search">
                        <i class="material-icons">search</i></label>
                        <br>
                        <i class="material-icons" style="margin-top: 8px;" ng-click="filtro_ot=''">close</i>
                    </div>
                </div>
                
                <ul class="collection" ng-init="vehiculos_orden()">
                    <li class="collection-item avatar" ng-repeat="dato in vehiculos_ot | filter:filtro_ot">
                        <i class="circle blue-grey center centrado_vert" style="margin-top: 10px; font-style: normal">
                            {{ dato.marca | limitTo:1 | uppercase }}{{ dato.modelo | limitTo:1 | uppercase }}
                        </i>
                        
                        <span class="title mayuscula">{{ dato.marca }} {{ dato.modelo }} | {{ dato.placa }} </span>
                        <p>
                            Recorrido Actual: {{ dato.recorrido }}
                            
                            <br>
                            
                            <a class="blue-grey-text mayuscula manita" ng-click="filtro_ot = dato.nombre_cliente">
                                Dueño: {{ dato.nombre_cliente }} ( {{ dato.identificacion_cliente }} )
                            </a>
                        </p>
                        
                        <a href="#!" class="secondary-content tooltipped" data-position="bottom" data-delay="50" data-tooltip="Seleccionar a {{ dato.modelo }}" ng-click="configurar_ot(1, dato.id_vehiculo)">
                            <i class="material-icons">send</i>
                        </a>
                    </li>
                </ul>
                
            </div>
            <div class="modal-footer">
                <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="modal_cerrar('modal1')">Cancelar</a>
            </div>
        </div>
        
        <!-- Modal Configuración OT -->
        <div id="modal2" class="modal modal-fixed-footer">
            <div class="modal-content">
                
                <h4>
                    <blockquote class="red-text text-lighten-1 mayuscula">
                        Confirgurar Orden de servicio
                    </blockquote>
                </h4>
                
                <div class="row">
                    <div class="section">
                        <div class="card-panel">
                            <table>
                                <thead>
                                  <tr class="mayuscula">
                                      <th>Motor</th>
                                      <th>Marca</th>
                                      <th>Modelo</th>
                                      <th>Placa</th>
                                      <th>color</th>
                                      <th>Recorrido</th>
                                      <th>Propietario</th>
                                  </tr>
                                </thead>

                                <tbody>
                                  <tr ng-repeat="vh in vehiculo_ot">
                                    <td>{{ vh.motor }}</td>
                                    <td>{{ vh.marca }}</td>
                                    <td>{{ vh.modelo }}</td>
                                    <td>{{ vh.placa }}</td>
                                    <td>{{ vh.color }}</td>
                                    <td>{{ vh.recorrido }}</td>
                                    <td>{{ vh.nombre_cliente }}</td>
                                  </tr>
                                </tbody>
                              </table>
                        </div>
                    </div>
                    
                    <div class="section">
                        <div class="input-field col s4">
                            <select id="tipo_servicio">
                                <option class="mayuscula" value="" disabled selected>Seleccione tipo de servicio</option>
                                <option value="Reacondicionamiento">Reacondicionamiento</option>
                                <option value="Diagnóstico">Diagnóstico</option>
                                <option value="Otro Servicio">Otro Servicio</option>
                                
                                <optgroup label="Mantenimientos">
                                    <option value="Mantenimiento Menor">Mantenimiento Menor</option>
                                    <option value="Mantenimiento Mayor">Mantenimiento Mayor</option>
                                </optgroup>
                                <optgroup label="Reparaciones">
                                    <option value="Reparación Menor">Reparación Menor</option>
                                    <option value="Reparación Mayor">Reparación Mayor</option>
                                </optgroup>
                                    
                                
                            </select>
                            <label>Tipo de Servicio</label>
                        </div>

                        <div class="input-field col s4">
                            <select multiple id="area_afectada">
                                <option class="mayuscula" value="" disabled selected>Seleccione Area Afectada</option>
                                <option value="Motor">Motor</option>
                                <option value="Transmisión">Transmisiónr</option>
                                <option value="Sistema Eléctrico">Sistema Eléctrico</option>
                                <option value="Sistema Elestrónico">Sistema Elestrónico</option>
                                <option value="Suspensión">Suspensión</option>
                                <option value="Dirección">Dirección</option>
                                <option value="Frenos">Frenos</option>
                            </select>
                            <label>Tipo de Servicio</label>
                        </div>

                        <div class="input-field col s4">
                            <select multiple id="tipo_trabajo">
                                <option class="mayuscula" value="" disabled selected>Seleccione Tipo de trabajo</option>
                                <option value="Mecánico">Mecánico</option>
                                <option value="Electrico">Electrico</option>
                                <option value="Electronico">Electronico</option>
                                <option value="Electromecanico">Electromecanico</option>
                            </select>
                            <label>Tipo de Trabajo</label>
                        </div>
                    </div>
                    
                    <div class="section">
                        <dov class="col s8 input-field">
                            <input placeholder="Ingrese Recorrido" id="recorrido_ot" type="text" ng-model="_recorrido" />
                            <label for="recorrido_ot">Nuevo Recorrido</label>
                        </dov>
                        <div class="col s4 input-field">
                            <input placeholder="Ingrese costo" id="costo_ot" type="text" ng-model="costo_ot" />
                            <label for="costo_ot">Costo de Orden {{ costo_ot | currency: 'C$':0 }}</label>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <a class="modal-action waves-effect waves-green btn-flat" ng-click="configurar_ot(2, false)">
                    <i class="material-icons centrado_vert">folder_open</i>
                    Abrir Orden
                </a>
            </div>
        </div>
        
        <!-- Modal Resumen Ot -->
        <div id="modal3" class="modal modal-fixed-footer">
            <div class="modal-content" ng-repeat="dato in dato_ot">
                
                <h4>
                    <blockquote class="red-text text-lighten-1 mayuscula">
                        {{ dato.tipo_servicio }} en {{ dato.area_afectada }}
                    </blockquote>
                </h4>

                <div class="section">
                    <table class="centered bordered">
                        <thead>
                            <tr class="mayuscula">
                                <th>Tipo de servicio</th>
                                <th>Area Afectada</th>
                                <th>Tipo de Trabajo</th>
                                <th>Fecha de Orden</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>{{ dato.tipo_servicio }}</td>
                                <td>{{ dato.area_afectada }}</td>
                                <td>{{ dato.tipo_trabajo }}</td>
                                <td>{{ dato.hora }} {{ dato.fecha }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="section">
                    <div class="card-panel">                        
                        <h5 class="mayuscula center truncate">Dato del vehiculo</h5>
                        <table class="centered bordered">
                            <thead class="mayuscula">
                                <tr>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Placa</th>
                                    <th>Recorrido</th>
                                    <th>Dueño</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td>{{ dato.marca_vehiculo }}</td>
                                    <td>{{ dato.modelo_vehiculo }}</td>
                                    <td>{{ dato.placa_vehiculo }}</td>
                                    <td>{{ dato.recorrido_vehiculo }}</td>
                                    <td>{{ dato.nombre_cliente }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="section">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title mayuscula center truncate">Repuestos Utilizados</span>
                            
                            <ul class="collection">
                                <li class="collection-item avatar dismissable mayuscula" 
                                    ng-repeat="data in repuesto_ot">
                                    <div class="centrado_vert">
                                        <i class="circle teal darken-3 center centrado_vert" 
                                            style="margin-top: 10px; font-style: normal">
                                            {{ data.repuesto | limitTo:1 | uppercase }}
                                        </i>
                                        
                                        <span class="title">{{ data.repuesto }} - <b>{{ data.precio | currency: 'C$':00 }}</b></span>
                                        <p>
                                            <small>{{ data.descripcion }}</small>
                                            <br>
                                            <b>Cantidad: {{ data.cantidad }}</b>
                                            <br>
                                            <span class="teal-text">
                                               Codigo: {{ data.codigo }}
                                            </span>
                                        </p>

                                       <?php

                                       if($_SESSION['admin'] == 1)
                                       {
                                        ?>

                                        <a class="secondary-content teal-text text-darken-1 waves-effect tooltipped" 
                                            data-position="bottom" data-delay="50" 
                                            data-tooltip="Eliminar elemento" 
                                            ng-click="eliminar_repuesto(data.id, data.repuesto, data.id_ot)">
                                            <i class="material-icons">delete</i>
                                        </a>

                                        <?php
                                       }

                                       ?>
                                    </div>
                                </li>
                            </ul>                                
                        </div>
                            
                        <div class="card-action center">
                            <a href="#" ng-click="abrir_inventario_ot(data.id_ot)" 
                            class="waves-effect btn teal darken-3 waves-light">Agregar repuesto</a>
                        </div>
                    </div>
                </div>

                <!--
                <ul class="collapsible" data-collapsible="expandable">
                    <li>
                        <div class="collapsible-header">
                            <i class="material-icons">filter_drama</i>
                            Resumen de Orden de Trabajo
                        </div>
                        <div class="collapsible-body">
                            
                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header">
                            <i class="material-icons">filter_drama</i>
                            Resumen de Vehiculo
                        </div>

                        <div class="collapsible-body">
                            
                                
                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header">
                            <i class="material-icons">filter_drama</i>
                            Repuestos Utilizados
                        </div>

                        <div class="collapsible-body">
                            
                        </div>
                    </li>
                </ul>
                -->
                <div class="section">
                    <h5 class="teal-text text-darken-3 center mayuscula">
                        Monto total de orden {{ dato.costo | currency:'C$':00 }}
                    </h5>
                </div>
            </div>
                
            <div class="modal-footer">
                <a class="modal-action waves-effect waves-green btn-flat" ng-click="modal_cerrar('modal3')">
                    <i class="material-icons centrado_vert">close</i>
                    Cerrar
                </a>
            </div>
        </div>
        
        <!-- Modal Seleccion de repuesto  -->
        <div id="modal4" class="modal modal-fixed-footer">
            <div class="modal-content">             
                <div class="progress" ng-hide="inventario">
                  <div class="indeterminate"></div>
                </div>
                <div class="row centered">
                    <div class="col s6 offset-s6 input-field">
                        <input id="search" class="mayuscula" type="search" required placeholder="bucar en Inventario" ng-model="filtrador">
                        <label class="label-icon" for="search">
                        <i class="material-icons">search</i></label>
                        <br>
                        <i class="material-icons" style="margin-top: 8px;" ng-click="filtrador='';">close</i>
                    </div>
                    <div class="col s12">
                        <ul class="collection">
                            <li class="collection-item avatar dismissable mayuscula" ng-repeat="data in inventario | filter: filtrador" id="p_{{ data.id }}">
                                <div class="centrado_vert">
                                    <i class="circle teal darken-3 center centrado_vert" style="margin-top: 10px; font-style: normal">{{ data.nombre | limitTo:1 | uppercase }}</i>
                                    <span class="title">{{ data.nombre }} - <b>{{ data.precio | currency: 'C$':00 }}</b></span>
                                    <p>
                                        <small>{{ data.descripcion }}</small>
                                        <br>
                                        <b>Existencia: {{ data.existencia }}</b>
                                        <br>
                                        <span class="teal-text">
                                           Codigo: {{ data.codigo }}
                                        </span>
                                    </p>

                                    <a href="#!" class="secondary-content teal-text text-darken-1 waves-effect tooltipped" data-position="bottom" data-delay="50" data-tooltip="Agregar al carrito" ng-click="add_repuesto(data.id)">
                                        <i class="material-icons">add</i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div> 
                        
                
                
                <!-- Preloader -->
                <div class="preloader-wrapper small active" ng-hide="inventario" id="loader">
                    <div class="spinner-layer spinner-green-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="gap-patch">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
            <div class="modal-footer">
                <a class="modal-action modal-close waves-effect waves-green btn-flat" 
                ng-click="modal_cerrar('modal4'); modal('modal3'); ot_especifica()">Cerrar</a>
            </div>
        </div>
        
    <?php
}
else
{
    include "page/403.php";
}
?>
