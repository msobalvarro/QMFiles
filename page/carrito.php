<?php

require "php/paginas.php";

if($inventario_ == 1)
{
      
    ?>


    <div class="container white" ng-init="titulo='Carrito'; " ng-show="muestra_shop">

        <!-- Tabla de Productos a facturar -->
        <div class="section" id="SECT_HIDE_PDF">
            <table class="bordered responsive-table highlight">
                <thead>
                    <th>Cantidad</th>
                    <th>En Existencia</th>
                    <th>Código</th>
                    <th>Producto</th>
                    <th>Descripción</th>
                    <th>Precio</th>
                </thead>
                <tbody>
                    <tr ng-repeat="carro in productos_compra | filter: filtrador">
                        <td style="width: 150px">
                            <div class="input-field">
                                <input type="number" ng-change="total_general()" max="{{ carro.existencia }}" ng-model="carro.unidad" placeholder="Cantidad" />
                            </div>
                        </td>
                        <td>
                            {{ carro.existencia }}
                        </td>
                        <td>
                            {{ carro.codigo }}
                        </td>
                        <td>
                            {{ carro.nombre }}
                        </td>
                        <td>
                            {{ carro.descripcion }}
                        </td>
                        <td>
                            {{ (carro.precio * carro.unidad) | currency: 'C$':00 }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="right-align">
                <p>
                    <input type="checkbox" ng-init="iva_fac=true" ng-model="iva_fac" id="testIVA" ng-change="total_general()" />
                    <label for="testIVA" class="mayuscula">Generar IVA</label>
                </p>
            </div>
            <div class="section center">
                <h4 class="mayuscula teal-text text-darken-3" ng-init="total_general()">
                    total a Pagar: {{ total_g | currency: 'C$':00 }}
                </h4>
            </div>

            <div class="section right-align">
                <a href="./?url=inventario" class="btn btn-large grey lighten-2 grey-text text-darken-3 waves-effect">
                    Editar Compra
                    <i class="material-icons centrado_vert">edit</i>
                </a>
                <button class="btn btn-large amber waves-effect lighten-3 grey-text text-darken-3 mayuscula" ng-class="{disabled: btn_buy}" ng-click="facturar_compra()">
                    generar factura
                    <i class="material-icons centrado_vert">assignment_turned_in</i>
                </button>
            </div>
        </div>

        <div class="section" id="PDF_FACT">
            <a href="./?url=inventario" class="btn btn-large grey lighten-2 grey-text text-darken-3 waves-effect">
                Ir a Inventario
            </a>
            <h5 class="center-align mayuscula">Vea este registro en el maletín</h5>
            
        </div>

    </div>

    <!--        mensaje (no hay registros)-->
    <div class="section white" ng-hide="muestra_shop">
        <h4 class="grey-text center text-lighten-2 mayuscula">
            No hay registros que facturar
        </h4>
        <h6 class="grey-text center text-lighten-2 mayuscula">
            (Agregre Registro)
        </h6>

        <div class="section center">
            <a href="./?url=inventario" class="btn waves-effect">Ir a Inventario</a>
        </div>
    </div>


    <!--       modal-->
    <div id="modal4" class="modal modal-fixed-footer">
        <div class="modal-content">


            <ul class="collection with-header" ng-init="traer_clientes()">
                <li class="collection-header">
                    <h4>Seleccione un Dueño</h4>
                    <div class="row">
                        <input type="search" placeholder="filtar" ng-model="filtro">
                    </div>
                </li>
                <li class="collection-item" ng-repeat="client in clientes | filter: filtro ">
                    {{ client.nombres}} - <span class="grey-text">{{ client.identificacion }}</span>

                    <a class="secondary-content manita tooltipped" ng-click="cliente_seleccion(client.nombres, client.id_cliente); modal_cerrar('modal4'); desaparecer=false" data-position="left" data-delay="50" data-tooltip="Seleccionar a {{ client.nombres }}">
                            <i class="material-icons">send</i>
                        </a>

                </li>
            </ul>

        </div>

        <div class="modal-footer">
            <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="modal('modal2'); modal_cerrar('modal4')">
                    Agregar Cliente
                    <li class="material-icons centrado_vert">add</li>
                </button>

            <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="modal_cerrar('modal4')">
                    Cancelar
                    <li class="material-icons centrado_vert">close</li>
                </button>
        </div>
    </div>


    <?php
}
else
{
    ?>

        <h3 class="center red-text">Usted no tiene permiso para acceder</h3>


        <?php
}
?>
