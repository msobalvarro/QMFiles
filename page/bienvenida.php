<form id="formulario" action="php/nuevos_datos_empresa.php" method="post" enctype="multipart/form-data">
    
    <div class="slider fullscreen ">
        <ul class="slides">
            <li>
                <div class="caption">

                </div>
            </li>
            <li>
                <div class="caption center-align ">
                    <!--
                    <h1>
                        <i class="material-icons large">home</i>
                    </h1>
                    -->
                    <h3>Bienvenido a QM Files</h3>
                    <h5 class="light grey-text text-lighten-3">
                        Sigue los pasos para continuar
                    </h5>
                    <br>
                    <a href="#" class="waves-effect waves-light btn-large grey darken-3 next">
                      Siguiente
                    </a>
                </div>
            </li>
            <li>
                <div class="caption right-align">
                    <h3>Escribe el nombre de tu empresa</h3>
                    <div class="row">
                        <div class="col s6 offset-s6 input-field">
                            <input type="text" class="white black-text" placeholder="Nombre de Empresa" id="nombre_empresa" name="nombre">
                            <span for="empresa">Ingresa nombre de empresa</span>
                        </div>
                    </div>
                    <br>
                    <a href="#" class="waves-effect waves-light btn-large grey darken-3 next">
                      Siguiente
                    </a>
                </div>
            </li>
            <li>
                <div class="caption center-aling">
                    <h3>Elije un logo para tu empresa</h3>
                       
                    Selecciona un logo: <br />
                    
                    <div class="row">
                        <div class="file-field col s6 input-field">
                            <div class="btn grey">
                                <span>Seleccionar</span>
                                <input type="file" id="files" accept="image/*" name="logo">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                            <span>*No te preocupes si no posees uno*</span>
                        </div>
                    </div>
                    <br />
                    <style>
                        #list
                        {
                            width: 250px;
                        }
                        #list img
                        {
                            max-width: 100%;
                        }
                    </style>
                    <output id="list" class="materialboxed"></output>

                    <script>
                      function archivo(evt) {
                        var files = evt.target.files; // FileList object

                        // Obtenemos la imagen del campo "file".
                        for (var i = 0, f; f = files[i]; i++) {
                          //Solo admitimos imágenes.
                          if (!f.type.match('image.*')) {
                            continue;
                          }

                          var reader = new FileReader();

                          reader.onload = (function(theFile) {
                            return function(e) {
                              // Insertamos la imagen
                             document.getElementById("list").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                            };
                          })(f);

                          reader.readAsDataURL(f);
                        }
                      }

                      document.getElementById('files').addEventListener('change', archivo, false);
                    </script>


                    <br>
                    <a href="#" class="waves-effect waves-light btn-large grey darken-3 prev">
                      Atrás
                    </a>
                    <a href="#" id="finish" class="waves-effect waves-light btn-large grey darken-3">
                      Finalizar
                    </a>
                </div>
            </li>
        </ul>
    </div>
    
    <script type="text/javascript">
        $('.slider').slider();
        next();
        $('.indicators').remove();
        
        $('.next').click(next);
        $('.prev').click(prev);
        
        $('#finish').click(function(){
            if($('#nombre_empresa').val() == '')
            {
                prev();
                swal('Ingrese Nombre de empresa');
            }
            else
            {
                $('#formulario').submit();
            }
        });
        
        function next ()
        {
            $('.slider').slider('next');
            $('.slider').slider('pause');
        }
        
        function prev ()
        {
            $('.slider').slider('prev');
            $('.slider').slider('pause');
        }
        
    </script>
    
</form>