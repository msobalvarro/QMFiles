<div class="row alto-completo white">
    <div class="col s3 alto-completo grey lighten-5 z-depth-2 hide-on-med-and-down">        
        <div class="section">
            <form accept-charset="UTF-8" id="login" class="centered" method="post" action="json/login.php" role="form">
                <div class="center">
                    <img src="logos/logoqm.png" class="logo-login" draggable="false">
                    <h4 class="teal-text text-darken-3 mayuscula">Login</h4>
                </div>
                <div class="row">
                    <div class="col s12">
                        <input required class="form-control " placeholder="Usuario" name="usuario" type="text" autofocus ng-model="usuario">
                    </div>
                    <div class="col s12">
                        <input required class="form-control" placeholder="Clave" name="clave" type="password" ng-model="clave">
                    </div>
                </div>
                
                <div class="progress" ng-show="cargar">
                    <div class="indeterminate"></div>
                </div>
                
                <div class="center">
                    <a href="#" class="btn waves-effect waves-light btn-large teal darken-3" ng-click="login()">Login</a>
                </div>
            </form>
        </div>
    </div>

    <div class="s9 alto-completo scroll">
        <div class="section no-pad-bot teal" id="index-banner"> 
            <a class="button-collapse top-nav full hide-on-large-only" href="#" data-activates="slide-out">
                <i class="material-icons">menu</i>
            </a>
            <div class="container">
                <h1 class="header center-on-small-only center">QM FILES</h1>
                <div class="row center">
                    <h4 class="header col s12 light center">Una moderna herramienta para administrar su taller de servicio</h4>
                </div>
                <div class="row center">
                    <a class="btn-large teal teal lighten-2 waves-effect waves-light" id="download-button" href="#">
                    Contratar
                    </a>
                </div>
                <div class="row center">
                    <a class="teal-text text-lighten-4" href="#">Versión Beta 1.1</a>
                </div>
            </div>

            <div class="github-commit">
                <div class="container">
                    <div class="commit">
                        <a class="btn right white-text text-lighten-5 waves-effect waves-light hide-on-small-only" 
                        id="github-button" href="#">Nosotros</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="divider"></div>
        <div class="section">
            <h4 class="mayuscula grey-text text-darken-3 center">
                ellos Nos brinda su confianza
            </h4>
            <div class="carousel">
                <?php
                    include 'json/logos.php';
                ?>
            </div>
            
        </div>
        <div class="divider"></div>

        <footer class="page-footer left-align grey darken-3">
          <div class="container">
            <div class="row">
              <div class="col l6 s8">
                <h5 class="white-text">QM Files 2017</h5>
                <p class="grey-text text-lighten-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>
          </div>
          <div class="footer-copyright grey darken-2">
            <div class="container">
                <span class="mayuscula">© 2017 Copyright Todos los derechos reservados</span>
                <a class="grey-text text-lighten-4 right" href="#!">Mas de QM</a>
            </div>
          </div>
        </footer>
    </div>
    
    <div id="slide-out" class="side-nav">
        <form accept-charset="UTF-8" id="login2" class="centered" method="post" action="json/login.php" role="form">
            <div class="center">
                <img src="logos/logoqm.png" class="logo-login" draggable="false">
                <h4 class="teal-text text-darken-3 mayuscula">Login</h4>
            </div>
            <div class="row">
                <div class="col s12">
                    <input required class="form-control " placeholder="Usuario" name="usuario" type="text" autofocus ng-model="usuario">
                </div>
                <div class="col s12">
                    <input required class="form-control" placeholder="Clave" name="clave" type="password" ng-model="clave">
                </div>
            </div>
            
            <div class="progress" ng-show="cargar">
                <div class="indeterminate"></div>
            </div>
            
            <div class="center">
                <a href="#" class="btn waves-effect waves-light btn-large teal darken-3" ng-click="login()">Login</a>
            </div>
        </form>
    </div>
            
</div>
<style type="text/css">
    body
    {
        overflow: hidden;
    }
    .alto-completo
    {
        height: {{ alto_dispositivo() }}px;
    }
    footer{
        padding-left: 0;
    }
</style>
