<?php

require "php/paginas.php";

if($vehiculos_ == 1)
{
    ?>


        <div class="container" ng-init="titulo='Registro de Vehiculos'; traer_clientes(); traer_vehiculo();">
            <div class="progress" ng-hide="vehiculos">
              <div class="indeterminate"></div>
            </div>

            <ul class="collapsible popout" data-collapsible="expandable">
                <li class="collection-item selection" ng-repeat="vehiculo in vehiculos | filter: filtrador">
                    <div class="collapsible-header teal-text text-darken-1">
                        <i class="material-icons Medium">folder</i>
                        <div class="cont-secundary">
                            <?php

                            if($_SESSION['admin'] == 1)
                            {
                                ?>
                                <a ng-click="borrar_vehiculo(vehiculo.id_vehiculo, vehiculo.marca)" class="secondary-content tooltipped" data-position="bottom" data-delay="50" data-tooltip="Eliminar">
                                    <i class="material-icons icon-le">delete</i>
                                </a>
                                <?php
                            }

                            ?>

                            <a ng-click="" class="secondary-content tooltipped" data-position="top" data-delay="50" data-tooltip="Descargar PDF">
                                <i class="material-icons icon-le">play_for_work</i>
                            </a>

                            <?php

                            if($_SESSION['admin'] == 1)
                            {
                                ?>
                                <a ng-click="edit_vehiculo(vehiculo.id_vehiculo)" class="secondary-content tooltipped" data-position="left" data-delay="50" data-tooltip="Modificar">
                                    <i class="material-icons icon-le">edit</i>
                                </a>
                                <?php
                            }

                            ?>
                        </div>
                        {{ vehiculo.marca | uppercase }} {{ vehiculo.modelo | uppercase }} 
                    </div>
                    <div class="collapsible-body grey lighten-4">

                        <table class="responsive-table">
                            <thead>
                                <tr class="mayuscula">
                                    <th>Placa</th>
                                    <th>Transmisión</th>
                                    <th>Cilindros</th>
                                    <th>Tipo de Motor</th>
                                    <th>Tipo de Uso</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td>{{ vehiculo.placa }}</td>
                                    <td>{{ vehiculo.transmicion }}</td>
                                    <td>{{ vehiculo.cilindros }}</td>
                                    <td>{{ vehiculo.tipo_motor }}</td>
                                    <td>{{ vehiculo.uso }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="section right-align">
                            <a class="secondary-content manita tooltipped" data-position="left" data-delay="50" data-tooltip="Preview" ng-click="dato_vehiculo_especifico(vehiculo.id_vehiculo)">
                                <i class="material-icons centrado_vert">visibility</i>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>

            <!-- Modal nuevo vehiculo -->
            <div id="modal1" class="modal modal-fixed-footer">
                <div class="modal-content">
                    <h4>
                        <blockquote class="red-text text-lighten-1 mayuscula">
                            Datos del Vehiculo
                        </blockquote>
                    </h4>

                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" required ng-model="vehiculo_marca" placeholder="Ingrese marca del vehiculo" id="marca">
                            <label for="marca">Marca del vehiculo</label>
                        </div>
                        <div class="input-field col s6">
                            <input type="text" required ng-model="vehiculo_modelo" placeholder="Ingrese modelo del vehiculo" id="modelo">
                            <label for="modelo">modelo del vehiculo</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" required ng-model="vehiculo_color" placeholder="Ingrese color del vehiculo" id="color">
                            <label for="color">color del vehiculo</label>
                        </div>
                        <div class="input-field col s6">
                            <input type="text" required ng-model="vehiculo_placa" placeholder="Ingrese placa del vehiculo" id="placa">
                            <label for="placa">placa del vehiculo</label>
                        </div>

                    </div>
                    <div class="row">
                       <div class="input-field col s6">
                            <input type="text" required ng-model="vehiculo_motor" placeholder="Ingrese motor del vehiculo" id="motor">
                            <label for="motor">motor del vehiculo</label>
                        </div>
                        <div class="input-field col s6">
                            <input type="text" required ng-model="vehiculo_chasis" placeholder="Ingrese chasis del vehiculo" id="chasis">
                            <label for="chasis">chasis del vehiculo</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" required ng-model="vehiculo_recorrido" placeholder="Ingrese recorrido del vehiculo" id="color">
                            <label for="recorrido">recorrido del vehiculo</label>
                        </div>
                        <div class="input-field col s6">
                            <select id="cilindros" required>
                                <option value="" disabled selected>Elija una cantidad de cilindros</option>
                                <option value="1 cilindro">1 cilindro</option>
                                <option value="2 cilindros">2 cilindros</option>
                                <option value="3 cilindros">3 cilindros</option>
                                <option value="4 cilindro/lineales">4 cilindros/lineales</option>
                                <option value="4 cilindros/opuestos">4 cilindros/opuestos</option>
                                <option value="5 cilindros">5 cilindros</option>
                                <option value="6 cilindros/lineales">6 cilindros/lineales</option>
                                <option value="6 cilindros/opuestos">6 cilindros/opuestos</option>
                                <option value="8 cilindros en V">8 cilindros en V</option>
                            </select>
                            <label>Cantidad de cilindro</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <select id="transmicion" required>
                                <option value="" disabled selected>Elija un tipo de transmición</option>
                                <option value="manual">manual</option>
                                <option value="automatica">automatica</option>
                            </select>
                            <label>Tipo de transmición</label>
                        </div>
                        <div class="input-field col s6">
                            <select id="tipo_motor" required>
                                <option value="" disabled selected>Elija un tipo de motor</option>
                                <option value="motor gasolina/convencional">motor gasolina/convencional</option>
                                <option value="motor gasolina/Inyectado">motor gasolina/Inyectado</option>
                                <option value="motor diesel/convencional">motor diesel/convencional</option>
                                <option value="motor diesel/Inyectado">motor diesel/Inyectado</option>
                                <option value="GLP">GLP</option>
                            </select>
                            <label>Tipo de motor</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <select id="tipo_uso" required>
                                <option value="" disabled selected>Elija un tipo de Uso</option>
                                <option value="Personal">Personal</option>
                                <option value="Selectivo">Selectivo</option>
                                <option value="Laboral">Laboral</option>
                            </select>
                            <label>Tipo de Uso</label>
                        </div>
                        <div class="input-field col s6 center">
                            <div class="card-panel">

                                <div class="chip" ng-init="cliente_seleccionado= '(Ningun Cliente seleccionado)';">
                                    {{ cliente_seleccionado }}
                                </div>
                                <input type="hidden" ng-model="id_cliente_sel" value="{{ id_cliente_seleccionado }}">
                            </div>


                            <a class="btn waves-effect waves-light teal darken-4 waves-green tooltipped" data-position="bottom" data-delay="50" data-tooltip="Nota: Si no existe cliente, Agregar cliente." ng-click="modal('modal4')">
                                Seleccionar Cliente
                                <i class="material-icons centrado_vert">add</i>
                            </a>
                        </div>

                    </div>
                </div>

                <div class="modal-footer center-aling">
                    <button ng-click="modal_cerrar('modal1')" class="waves-effect waves-green btn-flat">
                        Cancelar <i class="material-icons centrado_vert">close</i>
                    </button>
                    <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="add_vehiculo()">
                        Guardar
                        <li class="material-icons centrado_vert">check</li>
                    </button>

                </div>
            </div>

            <!-- Modal nuevo cliente -->
            <div id="modal2" class="modal modal-fixed-footer">
                <div class="modal-content">
                    <h4>
                        <blockquote class="red-text text-lighten-1 mayuscula">
                            Datos de cliente
                        </blockquote>
                    </h4>

                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" required ng-model="cliente_nombre" placeholder="Ingrese nombre de cliente" id="nombres">
                            <label for="nombres">Nombres de Cliente</label>
                        </div>
                        <div class="input-field col s6">
                            <select id="client_tipo" required>
                               <option value="" disabled selected>Elija un tipo de Cliente</option>
                                <option value="Particular">Particular</option>
                                <option value="Empresa">Empresa</option>
                            </select>
                            <label>Tipo de Cliente</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" required ng-model="cliente_identificacion" placeholder="Ingrese identificacion" id="identificacion">
                            <label for="identificacion">Identificación de Cliente</label>
                        </div>
                        <div class="input-field col s6">
                            <select id="client_tipo_id" required>
                                <option value="" disabled selected>Elija un tipo de identifiación</option>
                                <option value="Cedula">Cedula</option>
                                <option value="Pasaporte">Pasaporte</option>
                                <option value="Licencia de conducir">Licencia de conducir</option>
                                <option value="Carnet de seguros social">Carnet de seguros social</option>
                                <option value="Otro documento">Otro documento</option>
                            </select>
                            <label>Tipo de identificacion</label>
                        </div>
                    </div>
                    <div class="row">
                       <div class="input-field col s6">
                            <input type="text" required ng-model="cliente_telefono" placeholder="Ingrese telefono" id="telefono">
                            <label for="telefono">Telefono de Cliente</label>
                        </div>
                        <div class="input-field col s6">
                            <textarea id="direccion" required ng-model="cliente_direccion" class="materialize-textarea" data-length="255"></textarea>
                            <label for="direccion">dirección de Cliente</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="email" required ng-model="cliente_email" placeholder="Ingrese emali de cliente" class="validate" id="email">
                            <label for="email">Email de Cliente</label>
                        </div>
                        <div class="input-field col s6">
                            <input type="tel" required ng-model="cliente_emergencia" placeholder="Ingrese telefono de emergencia" id="emergencia">
                            <label for="emergencia">Contacto de emergencia</label>
                        </div>
                    </div>
                    </div>
                <div class="modal-footer center-aling">
                    <button ng-click="modal_cerrar('modal2')" class="waves-effect waves-green btn-flat">
                        Cancelar <i class="material-icons centrado_vert">close</i>
                    </button>
                    <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="add_cliente(); modal_cerrar('modal2')">
                        Guardar
                        <li class="material-icons centrado_vert">check</li>
                    </button>
                </div>
            </div>

            <!-- Modal resumen vehiculo -->
            <div id="modal3" class="modal modal-fixed-footer">
                <div ng-repeat="vh in vehiculo_especifico">
                    <div class="modal-content">
                        <div class="section">
                           <blockquote class="red-text text-lighten-1 mayuscula">
                                <h5>Dueño del vehiculo: {{ vh.nombre_cliente }}</h5>
                            </blockquote>
                        </div>

                        <div class="divider"></div>

                        <div class="section">
                            <table class="bordered responsive-table centered">
                                <thead>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Color</th>
                                    <th>Placa</th>
                                    <th>Motor</th>
                                    <th>Chasis</th>
                                </thead>
                                <tbody>
                                    <td>{{ vh.marca }}</td>
                                    <td>{{ vh.modelo }}</td>
                                    <td>{{ vh.color }}</td>
                                    <td>{{ vh.placa }}</td>
                                    <td>{{ vh.motor }}</td>
                                    <td>{{ vh.chasis }}</td>
                                </tbody>
                            </table>
                        </div>


                        <div class="section">
                            <table class="bordered responsive-table centered">
                                <thead>
                                    <th>Recorrido</th>
                                    <th>Cilindros</th>
                                    <th>Transmición</th>
                                    <th>Tipo de Motor</th>
                                    <th>Uso</th>
                                </thead>
                                <tbody>
                                    <td>{{ vh.recorrido }}</td>
                                    <td>{{ vh.cilindros }}</td>
                                    <td>{{ vh.transmicion }}</td>
                                    <td>{{ vh.tipo_motor }}</td>
                                    <td>{{ vh.uso }}</td>
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button ng-click="modal_cerrar('modal3')" class="waves-effect waves-green btn-flat">
                            Cerar <i class="material-icons centrado_vert">close</i>
                        </button>

                        <?php

                        if($_SESSION['admin'] == 1)
                        {
                            ?>
                            <button ng-click="modal_cerrar('modal3'); edit_vehiculo(vh.id_vehiculo)" class="waves-effect waves-green btn-flat">
                                Modificar <i class="material-icons centrado_vert">edit</i>
                            </button>
                            <?php
                        }

                        ?>

                    </div>
                </div>
            </div>

            <!-- Modal lista de clientes -->
            <div id="modal4" class="modal modal-fixed-footer">
                <div class="modal-content">


                    <ul class="collection with-header" ng-init="traer_clientes()">
                        <li class="collection-header">
                            <h4>Seleccione un Dueño</h4>
                            <div class="row">
                                <input type="search" placeholder="filtar" ng-model="filtro">
                            </div>
                        </li>
                        <li class="collection-item" ng-repeat="client in clientes | filter: filtro ">
                            {{ client.nombres}}

                            <a class="secondary-content manita tooltipped" ng-click="cliente_seleccion(client.nombres, client.id_cliente); modal_cerrar('modal4'); c_c=true" data-position="left" data-delay="50" data-tooltip="Seleccionar a {{ client.nombres }}">
                                <i class="material-icons">send</i>
                            </a>

                        </li>
                    </ul>

                </div>

                <div class="modal-footer">
                    <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="modal('modal2'); modal_cerrar('modal4')">
                        Agregar Cliente
                        <li class="material-icons centrado_vert">add</li>
                    </button>

                    <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="modal_cerrar('modal4')">
                        Cancelar
                        <li class="material-icons centrado_vert">close</li>
                    </button>
                </div>
            </div>

            <!-- Modal editar vehiculo -->
            <div id="modal5" class="modal modal-fixed-footer">
                <div ng-repeat="vh in vehiculo_especifico">
                    <div class="modal-content">
                        <div class="section">
                           <blockquote class="red-text text-lighten-1 mayuscula">
                                <h5>Editar</h5>
                            </blockquote>
                        </div>

                        <div class="divider"></div>

                        <div class="section">
                            <div class="row">
                                <div class="col s6 input-field">
                                    <input type="text" id="vh_marca" value="{{ vh.marca }}" placeholder="Ingrese Marca del vehiculo">
                                    <label for="vh_marca" class="active">Marca</label>
                                </div>

                                <div class="col s6 input-field">
                                    <input type="text" id="vh_modelo" value="{{ vh.modelo }}" placeholder="Ingrese modelo del vehiculo">
                                    <label for="vh_modelor" class="active">Modelo</label>
                                </div>

                                <div class="col s6 input-field">
                                    <input type="text" id="vh_color" value="{{ vh.color }}" placeholder="Ingrese color del vehiculo">
                                    <label for="vh_color" class="active">Color</label>
                                </div>

                                <div class="col s6 input-field">
                                    <input type="text" id="vh_placa" value="{{ vh.placa }}" placeholder="Ingrese placa del vehiculo">
                                    <label for="vh_placa" class="active">Placa</label>
                                </div>

                                <div class="col s6 input-field">
                                    <input type="text" id="vh_motor" value="{{ vh.motor }}" placeholder="Ingrese motor del vehiculo">
                                    <label for="vh_placa" class="active">Motor</label>
                                </div>

                                <div class="col s6 input-field">
                                    <input type="text" id="vh_chasis" value="{{ vh.chasis }}" placeholder="Ingrese chasis del vehiculo">
                                    <label for="vh_chasis" class="active">Chasis</label>
                                </div>

                                <div class="col s6 input-field">
                                    <input type="text" id="vh_recorrido" value="{{ vh.recorrido }}" placeholder="Ingrese recorrido del vehiculo">
                                    <label for="vh_recorrido" class="active">Recorrido</label>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button ng-click="modal_cerrar('modal5')" class="waves-effect waves-green btn-flat">
                            Cancelar <i class="material-icons centrado_vert">close</i>
                        </button>

                        <?php

                        if($_SESSION['admin'] == 1)
                        {
                            ?>
                            <button ng-click="editar_vehiculo(vh.id_vehiculo)" class="waves-effect waves-green btn-flat">
                                Actualizar <i class="material-icons centrado_vert">check</i>
                            </button>
                            <?php
                        }

                        ?>

                    </div>
                </div>

            </div>

        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large waves-effect waves-light btn teal darken-3 tooltipped" data-position="left" data-delay="50" data-tooltip="Agregar Vehiculo" ng-click="modal('modal1')">
                <li class="material-icons centrado_vert">add</li>
            </a>
        </div>

    <?php
}
else
{
    ?>

        <h3 class="center red-text">Usted no tiene permiso para acceder</h3>


    <?php
}
?>
