<div id="modal402" class="modal" ng-init="modal('modal402')">
    <div class="modal-content">
        <h4>Ups! :(</h4>
        <p>
            Parece que tu administrador a desactivado esta cuenta. Contácta con tu administrador y pidele que active nuevamente este usuario.
        </p>
        <p>
            <small>En pocos segundos te redicionaremos.</small>
        </p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Ok</a>
    </div>
</div>

