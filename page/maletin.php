<?php

require "php/paginas.php";

if($orden_ == 1)
{
    ?>

        <div class="container" ng-init="titulo='Maletín'; hide_search=true">
            <div class="row">
                <div class="col s10">
                    <h4 class="teal-text mayuscula">Facturas</h4>
                    <div class="scrollspy" id="searchBill">
                        <div class="input-field center">
                            <form ng-submit="generate_bill()">
                                <input type="text" name="search-bill" required placeholder="Ingrese numero de factura" ng-model="num_bill">
                                <button type="submit" name="button" class="btn teal darken-1">Buscar</button>
                            </form>
                        </div>
                        <div class="section content-bill" id="responsiveBil">
                            <div class="progress" ng-show="preloader_bill">
                                <div class="indeterminate"></div>
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="col s2">
                <ul class="section table-of-contents">
                    <li><a href="#searchBill" class="active">Buscar Factura</a></li>
                    <li><a href="#" class="">Elemeneto</a></li>
                    <li><a href="#" class="">Elemeneto</a></li>
                </ul>
                </div>
            </div>
        </div>

    <?php
}
else
{
    include "page/403.php";
}
?>
