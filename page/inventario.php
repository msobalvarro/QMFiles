<?php

require "php/paginas.php";

if($inventario_ == 1)
{

    ?>


    <div class="container">
        <div class="progress" ng-hide="inventario">
            <div class="indeterminate"></div>
        </div>


        <div ng-init="titulo='Inventario'; traer_inventario()">
            <!-- Ventana Todos los productos -->
            <div class="row centered">
                <div class="col s12 m8 l8">
                    <ul class="collection">
                        <li class="collection-item avatar dismissable mayuscula" data-index="{{ $index }}" ng-repeat="data in inventario | filter: filtrador" id="p_{{ data.id }}">
                            <div class="centrado_vert">
                                <i class="circle teal darken-3 center centrado_vert" style="margin-top: 10px; font-style: normal">{{ data.nombre | limitTo:1 | uppercase }}</i>
                                <span class="title">{{ data.nombre }} - <b>{{ data.precio | currency: 'C$':00 }}</b></span>
                                <p>
                                    <small>{{ data.descripcion }} | En existencia: {{ data.existencia }}</small>
                                    <br>
                                    <span class="teal-text">
                                             Codigo: {{ data.codigo }}
                                        </span>
                                </p>

                                <a href="#!" id="{{ data.id }}icon_cop" ng-hide="eliminar_ac" class="secondary-content teal-text text-darken-1  tooltipped disabled" data-position="bottom" data-delay="50" data-tooltip="Agregar al carrito">
                                    <i class="material-icons" ng-hide="data.activo" ng-click="eliminar_elemento($index, data.id); data.activo=true">check</i>
                                    <i class="material-icons" ng-show="data.activo" ng-click="add_compra(data, data.id, $index);">shopping_cart</i>
                                </a>

                                <a href="#!" ng-show="eliminar_ac" class="secondary-content red-text text-lighten-1 waves-effect tooltipped" data-position="bottom" data-delay="50" data-tooltip="Eliminar del inventario">
                                    <i class="material-icons" ng-click="eliminar_inventario(data.id, data.nombre)">delete</i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="col hide-on-small-only m4 l4">
                    <div>
                        <ul class="collection with-header">
                            <li class="collection-header teal-text mayuscula center">
                                <h4>
                                    En Venta
                                    <i class="material-icons">shopping_cart</i>
                                </h4>
                                {{ load_more }}
                                <div class="center">
                                    <a class='dropdown-button waves-effect teal darken-3 btn-large' ng-show="existencia_inv" href='#' data-activates='dropdown1'>
                                        Opciones
                                        <i class="material-icons centrado_vert">settings</i>
                                    </a>
                                    <ul id='dropdown1' class='dropdown-content'>
                                        <li>
                                            <a href="#!" ng-click="eliminarItemsShop()">
                                                <i class="material-icons">delete_sweep</i>
                                                Quitar Todo
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="collection-item" ng-repeat="producto in productos_compra" id="c_{{ producto.id }}">
                                <div>
                                    <small>{{ producto.nombre }}</small>
                                    <br>
                                    <b>{{ producto.precio | currency:'C$' }} (UND) </b>
                                </div>
                            </li>
                            {{ comprobar_datos_carro() }}
                            <li>
                                <h4 class="grey-text text-lighten-2 center mayuscula" ng-hide="existencia_inv">
                                    No hay Productos
                                </h4>
                            </li>
                            <li>
                                <div class="row section center">
                                    <a class="btn waves-effect waves-light amber lighten-2 grey-text text-darken-3" ng-if="productos_compra != []" ng-click="facturar_carrito()">
                                        Facturar
                                       <i class="material-icons centrado_vert">shopping_cart</i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>


        </div>
    </div>



    <!-- modales -->

    <!-- Modal nuevo Producto -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>
                <blockquote class="red-text text-lighten-1 mayuscula">
                    Nuevo Producto
                </blockquote>
            </h4>

            <div class="row">
                <div class="input-field col s6">
                    <input type="text" ng-model="nombre" placeholder="Ingrese Nombre del Producto" id="nombre_producto">
                    <label for="nombre_producto">Producto</label>
                </div>
                <div class="input-field col s6">
                    <input type="text" ng-model="codigo" placeholder="Ingrese Código del Producto" id="codigo_producto">
                    <label for="codigo_producto">Código</label>
                </div>
                <div class="input-field col s6">
                    <input type="number" ng-model="precio" placeholder="Ingrese Precio del Producto" id="descripcion_producto">
                    <label for="descripcion_producto">Precio</label>
                </div>
                <div class="input-field col s6">
                    <input type="number" ng-model="cantidad" placeholder="Ingrese Cantidad del Producto" id="cantidad_producto">
                    <label for="cantidad_producto">Cantidad</label>
                </div>
                <div class="input-field col s12">
                    <input type="text" ng-model="descripcion" placeholder="Ingrese Descripción del Producto" id="descripcion_producto" data-length="255" maxlength="255">
                    <label for="descripcion_producto">Descripción</label>
                </div>
            </div>

        </div>

        <div class="modal-footer center-aling">
            <button ng-click="modal_cerrar('modal1')" class="waves-effect waves-green btn-flat">
                    Cancelar <i class="material-icons centrado_vert">close</i>
                </button>
            <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="add_inventario(); modal_cerrar('modal1')">
                    Guardar
                    <li class="material-icons centrado_vert">check</li>
                </button>

        </div>
    </div>

    <!-- botonera -->
    <div class="fixed-action-btn click-to-toggle">
        <a class="btn-floating btn-large waves-effect waves-light yellow darken-4 tooltipped pulse" data-position="left" data-delay="50" ng-show="eliminar_ac" data-tooltip="Finalizar" ng-click="eliminar_ac=false">
            <li class="material-icons centrado_vert">done</li>
        </a>

        <a class="btn-floating btn-large waves-effect waves-light btn teal darken-3 tooltipped" data-position="left" data-delay="50" ng-hide="eliminar_ac" data-tooltip="Acciones">
            <li class="material-icons centrado_vert">assignment</li>
        </a>

        <ul ng-hide="eliminar_ac">
            <li>
                <a class="btn-floating blue waves-effect waves-light tooltipped" data-position="left" data-delay="50" data-tooltip="Nuevo Inventario" ng-click="modal('modal1')"><i class="material-icons">add</i></a>
            </li>
            <?php
                if($_SESSION['admin'] == 1)
                {
                    ?>
                <li>
                    <a class="btn-floating red darken-3 waves-effect waves-light tooltipped" data-position="left" data-delay="50" data-tooltip="Eliminar Producto" ng-click="eliminar_ac=true">
                            <i class="material-icons">delete</i>
                        </a>
                </li>
                <?php
                }

                ?>
        </ul>
    </div>

    <?php
}
else
{
    include "page/403.php";
}
?>
