<?php

require "php/paginas.php";

if($clientes_ == 1)
{
    ?>

    <div class="container" ng-init="titulo='Registro de Clientes'; ">
        <div class="progress" ng-hide="clientes">
            <div class="indeterminate"></div>
        </div>
        <ul class="collapsible popout" data-collapsible="expandable" ng-init="traer_clientes()">
            <li class="collection-item selection" draggable="true" dropzone="move" ng-repeat="cliente in clientes | filter: filtrador">
                <div class="collapsible-header teal-text text-darken-1">
                    <div class="cont-secundary">
                        <?php

                            if($_SESSION['admin'] == 1)
                            {
                                ?>
                            <a ng-click="borrar_usuario(cliente.id_cliente, cliente.nombres)" class="secondary-content tooltipped" data-position="bottom" data-delay="50" data-tooltip="Eliminar">
                                <i class="material-icons icon-le">delete</i>
                            </a>

                            <?php
                            }

                            ?>
                            <a ng-click="" class="secondary-content tooltipped" data-position="top" data-delay="50" data-tooltip="Descargar PDF">
                                <i class="material-icons icon-le">play_for_work</i>
                            </a>

                            <?php

                            if($_SESSION['admin'] == 1)
                            {
                                ?>
                                    <a ng-click="modific_cliente(cliente.id_cliente)" class="secondary-content tooltipped" data-position="left" data-delay="50" data-tooltip="Modificar">
                                        <i class="material-icons icon-le">edit</i>
                                    </a>

                                    <?php
                            }

                            ?>
                    </div>
                    <i class="material-icons centrado_vert">person</i> {{ cliente.nombres | uppercase }}
                </div>
                <div class="collapsible-body grey lighten-4">

                    <table class="responsive-table">
                        <thead>
                            <tr class="mayuscula">
                                <th>Tipo de Cliente</th>
                                <th>Telefono</th>
                                <th>Email</th>
                                <th>Identificación</th>
                                <th>Tipo de Identificación</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>{{ cliente.tipo_cliente }}</td>
                                <td>{{ cliente.telefono }}</td>
                                <td>{{ cliente.email }}</td>
                                <td>{{ cliente.identificacion }}</td>
                                <td>{{ cliente.tipo_id }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="section">
                        <a class="secondary-content manita tooltipped" data-position="left" data-delay="50" data-tooltip="Preview" ng-click="dato_cliente_especifico(cliente.id_cliente);">
                            <i class="material-icons centrado_vert">visibility</i>
                        </a>
                    </div>

                </div>
            </li>
        </ul>
    </div>

    <!-- Modal nuevo cliente -->

    <div id="modal1" class="modal modal-fixed-footer" ng-init="config.registro = true;">
        <div class="modal-content">
            <h4>
                <blockquote class="red-text text-lighten-1 mayuscula">
                    Datos de cliente
                </blockquote>
            </h4>

            <div class="row">
                <div class="input-field col s6">
                    <input type="text" required ng-model="cliente_nombre" placeholder="Ingrese nombre de cliente" id="nombres">
                    <label for="nombres">Nombres de Cliente</label>
                </div>
                <div class="input-field col s6">
                    <select id="client_tipo" required>
                           <option value="" disabled selected>Elija un tipo de Cliente</option>
                            <option value="Particular">Particular</option>
                            <option value="Empresa">Empresa</option>
                        </select>
                    <label>Tipo de Cliente</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input type="text" required ng-model="cliente_identificacion" placeholder="Ingrese identificacion" id="identificacion">
                    <label for="identificacion">Identificación de Cliente</label>
                </div>
                <div class="input-field col s6">
                    <select id="client_tipo_id" required>
                            <option value="" disabled selected>Elija un tipo de identifiación</option>
                            <option value="Cedula">Cedula</option>
                            <option value="Pasaporte">Pasaporte</option>
                            <option value="Licencia de conducir">Licencia de conducir</option>
                            <option value="Carnet de seguros social">Carnet de seguros social</option>
                            <option value="Otro documento">Otro documento</option>
                        </select>
                    <label>Tipo de identificacion</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input type="text" required ng-model="cliente_telefono" placeholder="Ingrese telefono" id="telefono">
                    <label for="telefono">Telefono de Cliente</label>
                </div>
                <div class="input-field col s6">
                    <textarea id="direccion" required ng-model="cliente_direccion" class="materialize-textarea" data-length="255"></textarea>
                    <label for="direccion">dirección de Cliente</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input type="email" required ng-model="cliente_email" placeholder="Ingrese emali de cliente" class="validate" id="email">
                    <label for="email">Email de Cliente</label>
                </div>
                <div class="input-field col s6">
                    <input type="tel" required ng-model="cliente_emergencia" placeholder="Ingrese telefono de emergencia" id="emergencia">
                    <label for="emergencia">Contacto de emergencia</label>
                </div>
            </div>


        </div>
        <div class="modal-footer center-aling">
            <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="agregar_registro('0')">
                    Siguiente
                    <li class="material-icons centrado_vert">skip_next</li>
                </button>
            <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="modal_cerrar('modal1')" ng-show="config.registro">
                    Cancelar
                    <li class="material-icons centrado_vert">close</li>
                </button>
        </div>
    </div>

    <!-- Modal editar cliente -->
    <div id="modal2" class="modal modal-fixed-footer">
        <div ng-repeat="data in datos_clientes">
            <div class="modal-content">
                <h4>
                    <blockquote class="red-text text-lighten-1 mayuscula">
                        Editar
                    </blockquote>
                </h4>

                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" placeholder="Nombre" id="nombre_edit" value="{{ data.nombres }}">
                        <label for="nombre">Nombre de cliente</label>
                    </div>

                    <div class="input-field col s6">
                        <input type="text" placeholder="Telefono" id="telefono_edit" value="{{ data.telefono }}">
                        <label for="telefono">Telefono de cliente</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" placeholder="identificacion" id="identificacion_edit" value="{{ data.identificacion }}">
                        <label for="identificacion">Identificación de cliente</label>
                    </div>

                    <div class="input-field col s6">
                        <input type="email" placeholder="email" id="email_edit" value="{{ data.email }}" class="validate">
                        <label for="telefono">Email de cliente</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s4">
                        <input type="text" placeholder="emergencia_edit" id="emergencia_edit" value="{{ data.contacto_emergencia }}">
                        <label for="emergancia">Contácto de emergencia</label>
                    </div>

                    <div class="input-field col s8">
                        <input type="text" placeholder="text" id="direccion_edit" value="{{ data.direccion }}">
                        <label for="direccion">Dirección de cliente</label>
                    </div>
                </div>

                <div class="section" ng-init="cliente_vehiculo_especifico(data.id_cliente)">
                    <ul class="collection with-header">
                        <li class="collection-header center">
                            <h5 class="teal-text text-darken-1 mayuscula">Vehiculos de {{ data.nombres }}</h5>
                        </li>
                        <li class="collection-item" ng-repeat="dv in dato_vehiculo">
                            <div>
                                {{ dv.marca }} {{ dv.modelo }} {{ dv.color }}

                                <a class="secondary-content manita tooltipped" data-position="bottom" data-delay="50" data-tooltip="Eliminar" ng-click="borrar_vehiculo(dv.id_vehiculo, dv.marca)">
                                    <i class="material-icons">delete</i>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="center grey-text text-lighten-1">
                                Parece que haz llegado al fin de los registros
                            </div>
                        </li>
                    </ul>
                </div>

            </div>

            <div class="modal-footer center-aling">
                <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="modal_cerrar('modal2')">
                        Cerrar
                        <li class="material-icons centrado_vert">close</li>
                    </button>
                <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="editar_cliente(data.id_cliente)">
                        Actualizar
                        <li class="material-icons centrado_vert">check</li>
                    </button>
            </div>
        </div>
    </div>

    <!-- Modal resumen cliente -->
    <div id="modal3" class="modal modal-fixed-footer">
        <div ng-repeat="x in cliente_especifico">
            <div class="modal-content">
                <h4>
                    <blockquote class="red-text text-lighten-1 mayuscula">
                        Resumen de {{ x.nombres }}
                    </blockquote>
                </h4>

                <div class="section">
                    <table class="bordered z-depth-1 responsive-table centered white">
                        <thead>
                            <th>Tipo de Cliente</th>
                            <th>Ìdentificación</th>
                            <th>Telefono</th>
                            <th>Dirección</th>
                            <th>email</th>
                            <th>Contácto de emergencia</th>
                        </thead>
                        <tbody>
                            <td>{{ x.tipo_cliente }}</td>
                            <td>{{ x.identificacion }}</td>
                            <td>{{ x.telefono }}</td>
                            <td>{{ x.direccion }}</td>
                            <td>{{ x.email }}</td>
                            <td>{{ x.contacto_emergencia }}</td>
                        </tbody>
                    </table>
                </div>

                <div class="section" ng-init="cliente_vehiculo_especifico(x.id_cliente)">
                    <ul class="collection with-header">
                        <li class="collection-header center">
                            <h5 class="teal-text text-darken-1 mayuscula">Vehiculos de {{ x.nombres }}</h5>
                        </li>
                        <li class="collection-item" ng-repeat="dv in dato_vehiculo">
                            <div>
                                {{ dv.marca }} {{ dv.modelo }} {{ dv.color }}

                                <a class="secondary-content manita tooltipped" data-position="left" data-delay="50" data-tooltip="Preview" ng-click="dato_vehiculo_especifico(dv.id_vehiculo);">
                                    <i class="material-icons centrado_vert">visibility</i>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="center grey-text text-lighten-1">
                                Parece que haz llegado al fin de los registros
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="modal-footer center-aling">
                <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="modal_cerrar('modal3')">
                        Cerrar
                        <li class="material-icons centrado_vert">close</li>
                    </button>


                <?php

                    if($_SESSION['admin'] == 1)
                    {
                        ?>
                    <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="modific_cliente(x.id_cliente)">
                                Modificar
                                <li class="material-icons centrado_vert">edit</li>
                        </button>

                    <?php
                    }

                    ?>

            </div>
        </div>
    </div>

    <!-- Modal vehiculo cliente -->
    <div id="modal30" class="modal modal-fixed-footer">
        <div class="modal-content" ng-repeat="vh in vehiculo_especifico">
            <div class="section">
                <blockquote class="red-text text-lighten-1 mayuscula">
                    <h4>
                        Resumen Vehicular de {{ vh.nombre_cliente }}
                    </h4>
                </blockquote>
            </div>

            <div class="divider"></div>

            <div class="section">
                <table class="bordered responsive-table centered">
                    <thead>
                        <th>Marca</th>
                        <th>Modelo</th>
                        <th>Color</th>
                        <th>Placa</th>
                        <th>Motor</th>
                        <th>Chasis</th>
                    </thead>
                    <tbody>
                        <td>{{ vh.marca }}</td>
                        <td>{{ vh.modelo }}</td>
                        <td>{{ vh.color }}</td>
                        <td>{{ vh.placa }}</td>
                        <td>{{ vh.motor }}</td>
                        <td>{{ vh.chasis }}</td>
                    </tbody>
                </table>
            </div>


            <div class="section">
                <table class="bordered responsive-table centered">
                    <thead>
                        <th>Recorrido</th>
                        <th>Cilindros</th>
                        <th>Transmisión</th>
                        <th>Tipo de Motor</th>
                        <th>Uso</th>
                    </thead>
                    <tbody>
                        <td>{{ vh.recorrido }}</td>
                        <td>{{ vh.cilindros }}</td>
                        <td>{{ vh.transmicion }}</td>
                        <td>{{ vh.tipo_motor }}</td>
                        <td>{{ vh.uso }}</td>
                    </tbody>
                </table>
            </div>

        </div>

        <div class="modal-footer">
            <button ng-click="modal_cerrar('modal30')" class="waves-effect waves-green btn-flat">
                    Cerar <i class="material-icons centrado_vert">close</i>
                </button>

            <?php

                if($_SESSION['admin'] == 1)
                {
                    ?>
                <button ng-click="" class="waves-effect waves-green btn-flat">
                        Modificar <i class="material-icons centrado_vert">edit</i>
                    </button>
                <?php
                }

                ?>

        </div>
    </div>

    <!-- Registro vehiculo -->
    <div id="modal4" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>
                <blockquote class="red-text text-lighten-1 mayuscula">
                    Datos del Vehiculo
                </blockquote>
            </h4>

            <div class="row">
                <div class="input-field col s6">
                    <input type="text" required ng-model="vehiculo_marca" placeholder="Ingrese marca del vehiculo" id="marca">
                    <label for="marca">Marca del vehiculo</label>
                </div>
                <div class="input-field col s6">
                    <input type="text" required ng-model="vehiculo_modelo" placeholder="Ingrese modelo del vehiculo" id="modelo">
                    <label for="modelo">modelo del vehiculo</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input type="text" required ng-model="vehiculo_color" placeholder="Ingrese color del vehiculo" id="color">
                    <label for="color">color del vehiculo</label>
                </div>
                <div class="input-field col s6">
                    <input type="text" required ng-model="vehiculo_placa" placeholder="Ingrese placa del vehiculo" id="placa">
                    <label for="placa">placa del vehiculo</label>
                </div>

            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input type="text" required ng-model="vehiculo_motor" placeholder="Ingrese motor del vehiculo" id="motor">
                    <label for="motor">motor del vehiculo</label>
                </div>
                <div class="input-field col s6">
                    <input type="text" required ng-model="vehiculo_chasis" placeholder="Ingrese chasis del vehiculo" id="chasis">
                    <label for="chasis">chasis del vehiculo</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input type="text" required ng-model="vehiculo_recorrido" placeholder="Ingrese recorrido del vehiculo" id="color">
                    <label for="recorrido">recorrido del vehiculo</label>
                </div>
                <div class="input-field col s6">
                    <select id="cilindros" required>
                            <option value="" disabled selected>Elija una cantidad de cilindros</option>
                            <option value="1 cilindro">1 cilindro</option>
                            <option value="2 cilindros">2 cilindros</option>
                            <option value="3 cilindros">3 cilindros</option>
                            <option value="4 cilindro/lineales">4 cilindros/lineales</option>
                            <option value="4 cilindros/opuestos">4 cilindros/opuestos</option>
                            <option value="5 cilindros">5 cilindros</option>
                            <option value="6 cilindros/lineales">6 cilindros/lineales</option>
                            <option value="6 cilindros/opuestos">6 cilindros/opuestos</option>
                            <option value="8 cilindros en V">8 cilindros en V</option>
                        </select>
                    <label>Cantidad de cilindro</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <select id="transmicion" required>
                            <option value="" disabled selected>Elija un tipo de transmición</option>
                            <option value="manual">manual</option>
                            <option value="automatica">automatica</option>
                        </select>
                    <label>Tipo de transmición</label>
                </div>
                <div class="input-field col s6">
                    <select id="tipo_motor" required>
                            <option value="" disabled selected>Elija un tipo de motor</option>
                            <option value="motor gasolina/convencional">motor gasolina/convencional</option>
                            <option value="motor gasolina/Inyectado">motor gasolina/Inyectado</option>
                            <option value="motor diesel/convencional">motor diesel/convencional</option>
                            <option value="motor diesel/Inyectado">motor diesel/Inyectado</option>
                            <option value="GLP">GLP</option>
                        </select>
                    <label>Tipo de motor</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <select id="tipo_uso" required>
                            <option value="" disabled selected>Elija un tipo de Uso</option>
                            <option value="Personal">Personal</option>
                            <option value="Selectivo">Selectivo</option>
                            <option value="Laboral">Laboral</option>
                        </select>
                    <label>Tipo de Uso</label>
                </div>
            </div>
        </div>

        <div class="modal-footer center-aling">
            <button ng-click="agregar_registro('1')" class="waves-effect waves-green btn-flat">
                    Agregar vehiculo <i class="material-icons centrado_vert">done</i>
                </button>
            <button ng-click="" class="waves-effect waves-green btn-flat">
                    Cancelar <i class="material-icons centrado_vert">close</i>
                </button>

        </div>
    </div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large waves-effect waves-light btn teal darken-3 tooltipped" data-position="left" data-delay="50" data-tooltip="Agregar Cliente" ng-click="modal('modal1')">
            <li class="material-icons centrado_vert">add</li>
        </a>
    </div>


    <?php
}
else
{
    include "page/403.php";
}
?>
