<?php

require "php/paginas.php";

if($inicio_ == 1)
{
    ?>
    <div class="progress" ng-hide="true">
        <div class="indeterminate"></div>
    </div>
    <div class="container">
        <div class="shortcut-container">
            <div class="row">
                <div class="col s10">
                    <div class="input-field">
                        <input type="text" placeholder="BUSCAR EN QM" class="disabled" disabled>
                    </div>
                </div>
                <div class="col s2 centrado_vert">
                    <button class="btn btn-large teal disabled darken-3 waves-effect">
                        buscar
                        <li class="material-icons centrado_vert">search</li>
                    </button>
                </div>
            </div>

            <div class="shortcut" ng-click="modal('modal_CLIENTES')">
                <i class="material-icons icon-new">person_add</i> Nuevo cliente
            </div>
            <div class="shortcut" ng-click="modal('modal_vehiculo')">
                <i class="material-icons icon-new">create_new_folder</i> Nuevo Vehiculo
            </div>
            <div class="shortcut" ng-click="modal('modal_OT')">
                <i class="material-icons icon-new">library_add</i> Nueva Orden
            </div>
            <div class="shortcut disabled">
                <i class="material-icons icon-new">person_add</i> Nuevo Inventario
            </div>
            <div class="shortcut" ng-click="usuario_especifico(<?php echo $_SESSION['id_usuario'] ?>)">
                <i class="material-icons icon-new">person_add</i> Configurar Usuario
            </div>
        </div>


        <!--      MODAL CLIENTES  	-->

        <div id="modal_CLIENTES" class="modal modal-fixed-footer" ng-init="config.registro = true;">
            <div class="modal-content">
                <h4>
                    <blockquote class="red-text text-lighten-1 mayuscula">
                        Datos de cliente
                    </blockquote>
                </h4>

                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" required ng-model="cliente_nombre" placeholder="Ingrese nombre de cliente" id="nombres">
                        <label for="nombres">Nombres de Cliente</label>
                    </div>
                    <div class="input-field col s6">
                        <select id="client_tipo" required>
                           <option value="" disabled selected>Elija un tipo de Cliente</option>
                            <option value="Particular">Particular</option>
                            <option value="Empresa">Empresa</option>
                        </select>
                        <label>Tipo de Cliente</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" required ng-model="cliente_identificacion" placeholder="Ingrese identificacion" id="identificacion">
                        <label for="identificacion">Identificación de Cliente</label>
                    </div>
                    <div class="input-field col s6">
                        <select id="client_tipo_id" required>
                            <option value="" disabled selected>Elija un tipo de identifiación</option>
                            <option value="Cedula">Cedula</option>
                            <option value="Pasaporte">Pasaporte</option>
                            <option value="Licencia de conducir">Licencia de conducir</option>
                            <option value="Carnet de seguros social">Carnet de seguros social</option>
                            <option value="Otro documento">Otro documento</option>
                        </select>
                        <label>Tipo de identificacion</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" required ng-model="cliente_telefono" placeholder="Ingrese telefono" id="telefono">
                        <label for="telefono">Telefono de Cliente</label>
                    </div>
                    <div class="input-field col s6">
                        <textarea id="direccion" required ng-model="cliente_direccion" class="materialize-textarea" data-length="255"></textarea>
                        <label for="direccion">dirección de Cliente</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input type="email" required ng-model="cliente_email" placeholder="Ingrese emali de cliente" class="validate" id="email">
                        <label for="email">Email de Cliente</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="tel" required ng-model="cliente_emergencia" placeholder="Ingrese telefono de emergencia" id="emergencia">
                        <label for="emergencia">Contacto de emergencia</label>
                    </div>
                </div>


            </div>
            <div class="modal-footer center-aling">
                <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="agregar_registro('0', 'modal_CLIENTES', 'modal_vehiculo')">
                    Siguiente
                    <li class="material-icons centrado_vert">skip_next</li>
                </button>
                <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="modal_cerrar('modal_CLIENTES')" ng-show="config.registro">
                    Cancelar
                    <li class="material-icons centrado_vert">close</li>
                </button>
            </div>
        </div>

        <!--      /MODAL CLIENTES  	-->


        <!--      MODAL VEHICULOS  	-->

        <div id="modal_vehiculo" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>
                    <blockquote class="red-text text-lighten-1 mayuscula">
                        Datos del Vehiculo
                    </blockquote>
                </h4>

                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" required ng-model="vehiculo_marca" placeholder="Ingrese marca del vehiculo" id="marca">
                        <label for="marca">Marca del vehiculo</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="text" required ng-model="vehiculo_modelo" placeholder="Ingrese modelo del vehiculo" id="modelo">
                        <label for="modelo">modelo del vehiculo</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" required ng-model="vehiculo_color" placeholder="Ingrese color del vehiculo" id="color">
                        <label for="color">color del vehiculo</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="text" required ng-model="vehiculo_placa" placeholder="Ingrese placa del vehiculo" id="placa">
                        <label for="placa">placa del vehiculo</label>
                    </div>

                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" required ng-model="vehiculo_motor" placeholder="Ingrese motor del vehiculo" id="motor">
                        <label for="motor">motor del vehiculo</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="text" required ng-model="vehiculo_chasis" placeholder="Ingrese chasis del vehiculo" id="chasis">
                        <label for="chasis">chasis del vehiculo</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" required ng-model="vehiculo_recorrido" placeholder="Ingrese recorrido del vehiculo" id="color">
                        <label for="recorrido">recorrido del vehiculo</label>
                    </div>
                    <div class="input-field col s6">
                        <select id="cilindros" required>
                                <option value="" disabled selected>Elija una cantidad de cilindros</option>
                                <option value="1 cilindro">1 cilindro</option>
                                <option value="2 cilindros">2 cilindros</option>
                                <option value="3 cilindros">3 cilindros</option>
                                <option value="4 cilindro/lineales">4 cilindros/lineales</option>
                                <option value="4 cilindros/opuestos">4 cilindros/opuestos</option>
                                <option value="5 cilindros">5 cilindros</option>
                                <option value="6 cilindros/lineales">6 cilindros/lineales</option>
                                <option value="6 cilindros/opuestos">6 cilindros/opuestos</option>
                                <option value="8 cilindros en V">8 cilindros en V</option>
                            </select>
                        <label>Cantidad de cilindro</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <select id="transmicion" required>
                                <option value="" disabled selected>Elija un tipo de transmición</option>
                                <option value="manual">manual</option>
                                <option value="automatica">automatica</option>
                            </select>
                        <label>Tipo de transmición</label>
                    </div>
                    <div class="input-field col s6">
                        <select id="tipo_motor" required>
                                <option value="" disabled selected>Elija un tipo de motor</option>
                                <option value="motor gasolina/convencional">motor gasolina/convencional</option>
                                <option value="motor gasolina/Inyectado">motor gasolina/Inyectado</option>
                                <option value="motor diesel/convencional">motor diesel/convencional</option>
                                <option value="motor diesel/Inyectado">motor diesel/Inyectado</option>
                                <option value="GLP">GLP</option>
                            </select>
                        <label>Tipo de motor</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <select id="tipo_uso" required>
                                <option value="" disabled selected>Elija un tipo de Uso</option>
                                <option value="Personal">Personal</option>
                                <option value="Selectivo">Selectivo</option>
                                <option value="Laboral">Laboral</option>
                            </select>
                        <label>Tipo de Uso</label>
                    </div>
                    <div class="input-field col s6 center">
                        <div class="card-panel">

                            <div class="chip" ng-init="cliente_seleccionado= '(Ningun Cliente seleccionado)';">
                                {{ cliente_seleccionado }}
                            </div>
                            <input type="hidden" ng-model="id_cliente_sel" value="{{ id_cliente_seleccionado }}">
                        </div>


                        <a class="btn waves-effect waves-light teal darken-4 waves-green tooltipped" data-position="bottom" data-delay="50" data-tooltip="Nota: Si no existe cliente, Agregar cliente." ng-click="modal('modal_SELECTCLIENT')">
                                Seleccionar Cliente
                                <i class="material-icons centrado_vert">add</i>
                            </a>
                    </div>

                </div>
            </div>

            <div class="modal-footer center-aling">
                <button ng-click="modal_cerrar('modal_vehiculo')" class="waves-effect waves-green btn-flat">
                    Cancelar <i class="material-icons centrado_vert">close</i>
                </button>
                <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="add_vehiculo(); modal_cerrar('modal_vehiculo')">
                    Guardar
                    <li class="material-icons centrado_vert">check</li>
                </button>

            </div>
        </div>


        <div id="modal_SELECTCLIENT" class="modal modal-fixed-footer">
            <div class="modal-content">


                <ul class="collection with-header" ng-init="traer_clientes()">
                    <li class="collection-header">
                        <h4>Seleccione un Dueño</h4>
                        <div class="row">
                            <input type="search" placeholder="filtar" ng-model="filtro">
                        </div>
                    </li>
                    <li class="collection-item" ng-repeat="client in clientes | filter: filtro ">
                        {{ client.nombres}}

                        <a class="secondary-content manita tooltipped" ng-click="cliente_seleccion(client.nombres, client.id_cliente); modal_cerrar('modal_SELECTCLIENT'); c_c=true" data-position="left" data-delay="50" data-tooltip="Seleccionar a {{ client.nombres }}">
                            <i class="material-icons">send</i>
                        </a>

                    </li>
                </ul>

            </div>

            <div class="modal-footer">
                <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="modal('modal2'); modal_cerrar('modal4')">
                        Agregar Cliente
                        <li class="material-icons centrado_vert">add</li>
                    </button>

                <button class="btn waves-effect waves-light btn-flat waves-green" ng-click="modal_cerrar('modal4')">
                        Cancelar
                        <li class="material-icons centrado_vert">close</li>
                    </button>
            </div>
        </div>

        <!--      /MODAL VEHICULOS  	-->

        <!--      MODAL OT  	-->

        <div id="modal_OT" class="modal modal-fixed-footer">
            <div class="modal-content">

                <!-- Preloader -->

                <div class="preloader-wrapper small active" ng-hide="vehiculos_ot" id="loader">
                    <div class="spinner-layer spinner-green-only">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="gap-patch">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col s8">
                        <h4>
                            <blockquote class="red-text text-lighten-1 mayuscula">
                                Seleccione Vehiculo
                            </blockquote>
                        </h4>
                    </div>

                    <div class="input-field col s4">
                        <input id="search" class="mayuscula" type="search" required placeholder="Filtrar" ng-model="filtro_ot">
                        <label class="label-icon" for="search">
                        <i class="material-icons">search</i></label>
                        <br>
                        <i class="material-icons" style="margin-top: 8px;" ng-click="filtro_ot=''">close</i>
                    </div>
                </div>

                <ul class="collection" ng-init="vehiculos_orden()">
                    <li class="collection-item avatar" ng-repeat="dato in vehiculos_ot | filter:filtro_ot">
                        <i class="circle blue-grey center centrado_vert" style="margin-top: 10px; font-style: normal">
                            {{ dato.marca | limitTo:1 | uppercase }}{{ dato.modelo | limitTo:1 | uppercase }}
                        </i>

                        <span class="title mayuscula">{{ dato.marca }} {{ dato.modelo }} | {{ dato.placa }} </span>
                        <p>
                            Recorrido Actual: {{ dato.recorrido }}

                            <br>

                            <a class="blue-grey-text mayuscula manita" ng-click="filtro_ot = dato.nombre_cliente">
                                Dueño: {{ dato.nombre_cliente }} ( {{ dato.identificacion_cliente }} )
                            </a>
                        </p>

                        <a href="#!" class="secondary-content tooltipped" data-position="bottom" data-delay="50" data-tooltip="Seleccionar a {{ dato.modelo }}" ng-click="configurar_ot(1, dato.id_vehiculo, 'modal_OTCONFIG', 'modal_OT')">
                            <i class="material-icons">send</i>
                        </a>
                    </li>
                </ul>

            </div>
            <div class="modal-footer">
                <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="modal_cerrar('modal_OT')">Cancelar</a>
            </div>
        </div>

        <!-- Modal Configuración OT -->
        <div id="modal_OTCONFIG" class="modal modal-fixed-footer">
            <div class="modal-content">

                <h4>
                    <blockquote class="red-text text-lighten-1 mayuscula">
                        Confirgurar Orden de servicio
                    </blockquote>
                </h4>

                <div class="row">
                    <div class="section">
                        <div class="card-panel">
                            <table>
                                <thead>
                                    <tr class="mayuscula">
                                        <th>Motor</th>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Placa</th>
                                        <th>color</th>
                                        <th>Recorrido</th>
                                        <th>Propietario</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr ng-repeat="vh in vehiculo_ot">
                                        <td>{{ vh.motor }}</td>
                                        <td>{{ vh.marca }}</td>
                                        <td>{{ vh.modelo }}</td>
                                        <td>{{ vh.placa }}</td>
                                        <td>{{ vh.color }}</td>
                                        <td>{{ vh.recorrido }}</td>
                                        <td>{{ vh.nombre_cliente }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="section">
                        <div class="input-field col s4">
                            <select id="tipo_servicio">
                                <option class="mayuscula" value="" disabled selected>Seleccione tipo de servicio</option>
                                <option value="Reacondicionamiento">Reacondicionamiento</option>
                                <option value="Diagnóstico">Diagnóstico</option>
                                <option value="Otro Servicio">Otro Servicio</option>

                                <optgroup label="Mantenimientos">
                                    <option value="Mantenimiento Menor">Mantenimiento Menor</option>
                                    <option value="Mantenimiento Mayor">Mantenimiento Mayor</option>
                                </optgroup>
                                <optgroup label="Reparaciones">
                                    <option value="Reparación Menor">Reparación Menor</option>
                                    <option value="Reparación Mayor">Reparación Mayor</option>
                                </optgroup>


                            </select>
                            <label>Tipo de Servicio</label>
                        </div>

                        <div class="input-field col s4">
                            <select multiple id="area_afectada">
                                <option class="mayuscula" value="" disabled selected>Seleccione Area Afectada</option>
                                <option value="Motor">Motor</option>
                                <option value="Transmisión">Transmisiónr</option>
                                <option value="Sistema Eléctrico">Sistema Eléctrico</option>
                                <option value="Sistema Elestrónico">Sistema Elestrónico</option>
                                <option value="Suspensión">Suspensión</option>
                                <option value="Dirección">Dirección</option>
                                <option value="Frenos">Frenos</option>
                            </select>
                            <label>Tipo de Servicio</label>
                        </div>

                        <div class="input-field col s4">
                            <select multiple id="tipo_trabajo">
                                <option class="mayuscula" value="" disabled selected>Seleccione Tipo de trabajo</option>
                                <option value="Mecánico">Mecánico</option>
                                <option value="Electrico">Electrico</option>
                                <option value="Electronico">Electronico</option>
                                <option value="Electromecanico">Electromecanico</option>
                            </select>
                            <label>Tipo de Trabajo</label>
                        </div>
                    </div>

                    <div class="section">
                        <dov class="col s8 input-field">
                            <input placeholder="Ingrese Recorrido" id="recorrido_ot" type="text" ng-model="_recorrido" />
                            <label for="recorrido_ot">Nuevo Recorrido</label>
                        </dov>
                        <div class="col s4 input-field">
                            <input placeholder="Ingrese costo" id="costo_ot" type="text" ng-model="costo_ot" />
                            <label for="costo_ot">Costo de Orden {{ costo_ot | currency: 'C$':0 }}</label>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <a class="modal-action waves-effect waves-green btn-flat" ng-click="configurar_ot(2, false,); modal_cerrar('modal_OTCONFIG')">
                    <i class="material-icons centrado_vert">folder_open</i> Abrir Orden
                </a>
            </div>
        </div>
        <!--      /MODAL OT  	-->


        <!--        MODAL CONFIGURACIÓN        -->

        <div id="modal1" class="modal modal-fixed-footer">
            <div ng-repeat="data in _usuario">
                <div class="modal-content">
                    <div class="row">
                        <div class="col s8">
                            <blockquote>
                                <h5 class="red-text text-lighten-1 mayuscula">
                                    General
                                </h5>
                            </blockquote>
                        </div>
                        <div class="col s4" ng-hide="data.mensaje">
                            <br>
                            <div class="switch centrado_vert">
                                <label>
                                            Inactivo
                                            <input type="checkbox" ng-click="cambiar_activo(data.id)" ng-checked="{{ data.activo }}" id="activo_check" />
                                            <span class="lever"></span>
                                            Activo
                                        </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s8">
                            <input placeholder="Ingrese nombre de usuario" id="usuario" type="text" ng-model="data.usuario">
                            <label class="active">Nombre de Usuario</label>
                        </div>
                        <div class="input-field col s4 center">
                            <a class="waves-effect waves-light btn blue tooltipped" data-position="right" data-delay="50" data-tooltip="Comprobar y Cambiar" ng-click="cambiar_nombre_usuario(data.id, data.usuario)">
                                        Cambiar
                                        <i class="material-icons centrado_vert">swap_horiz</i>
                                    </a>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="section">
                        <h6 class="grey-text text-darken-3 mayuscula">
                            <i class="material-icons centrado_vert">lock</i> Cambio de Contraseña
                        </h6>
                        <br>
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Ingrese Nueva Contraseña" id="c_nueva" type="password" ng-model="clave_nueva">
                                <label class="active mayuscula">Nueva Contraseña Para {{ data.usuario }}</label>
                            </div>
                            <div class="input-field col s6">
                                <input placeholder="Ingrese Contraseña de <?php echo $_SESSION['usuario'] ?>" id="c_actual" type="password" ng-model="clave_actual">
                                <label class="active mayuscula">Contraseña del usuario actual</label>
                            </div>
                            <div class="col s12 center">
                                <a class="waves-effect waves-light btn grey darken-3" ng-click="cambio_clave_usuario(data.id)">Cambiar</a>
                            </div>
                        </div>
                    </div>

                    <div class="divider"></div>

                    <!-- Credenciales configuracion -->
                    <blockquote>
                        <h5 class="red-text text-lighten-1 mayuscula">
                            Credenciales
                        </h5>
                    </blockquote>

                    <div class="progress" ng-hide="ventanas_activas">
                        <div class="indeterminate"></div>
                    </div>

                    <div class="center section" ng-if="data.mensaje">

                        <h4 class="grey-text center text-lighten-2 mayuscula">
                            Es administrador
                        </h4>
                        <h6 class="grey-text center text-lighten-2 mayuscula">
                            (No necesita permisos)
                        </h6>
                    </div>

                    <div class="section" ng-hide="data.mensaje">
                        <div ng-repeat="vent in ventanas">
                            <table class="bordered highlight mayuscula responsive-table">
                                <thead>
                                    <th>Acceso a</th>
                                    <th>Permitir</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            inicio
                                        </td>
                                        <td>
                                            <p>
                                                <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="inicio" id="check_inicio" ng-checked="{{vent.inicio}}" />
                                                <label for="check_inicio">Permitido</label>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Clientes
                                        </td>
                                        <td>
                                            <p>
                                                <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="clientes" id="check_clientes" ng-checked="{{vent.clientes}}" />
                                                <label for="check_clientes">Permitido</label>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            vehiculos
                                        </td>
                                        <td>
                                            <p>
                                                <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="vehiculos" id="check_vehiculos" ng-checked="{{vent.vehiculos}}" />
                                                <label for="check_vehiculos">Permitido</label>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Orden de servicio
                                        </td>
                                        <td>
                                            <p>
                                                <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="orden" id="check_orden" ng-checked="{{vent.orden}}" />
                                                <label for="check_orden">Permitido</label>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Inventario
                                        </td>
                                        <td>
                                            <p>
                                                <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="inventario" id="check_inventario" ng-checked="{{vent.inventario}}" />
                                                <label for="check_inventario">Permitido</label>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Configuración
                                        </td>
                                        <td>
                                            <p>
                                                <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="configuracion" id="check_configuracion" ng-checked="{{vent.configuracion}}" />
                                                <label for="check_configuracion">Permitido</label>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" ng-click="modal_cerrar('modal1')" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
                </div>
            </div>
        </div>


    </div>

    <?php
/*
    echo $_SERVER['PHP_SELF'];
    echo "<br>";
    echo $_SERVER['SERVER_NAME'];
    echo "<br>";
    echo $_SERVER['HTTP_HOST'];
    echo "<br>";
    echo $_SERVER['HTTP_REFERER'];
    echo "<br>";
    echo $_SERVER['HTTP_USER_AGENT'];
    echo "<br>";
    echo $_SERVER['SCRIPT_NAME'];
    echo "<br>";
    echo gethostname();
    echo "<br>";
    echo php_uname('v');
    */
    //echo get_browser();


}
else
{
    include "page/403.php";
}
?>
