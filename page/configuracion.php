<?php

require "php/paginas.php";

if($configuracion_ == 1)
{
    ?>
        


        <div class="container" ng-init="titulo='Configuración'; ">
            
            <?php
            
            if($_SESSION['admin'] == 1)
            {
                ?>
                <div class="divider"></div>
                <div class="progress" ng-hide="usuarios">
                  <div class="indeterminate"></div>
                </div>
                
                <div class="section white z-depth-2">
                    <h5 class="teal-text text-darken-2 center mayuscula">Configurar los usuarios</h5>
                    <table class="bordered centered highlight mayuscula responsive-table">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Tipo</th>
                                <th>activo</th>
                            </tr>
                        </thead>

                        <tbody ng-init="traer_usuarios()">
                            <tr ng-repeat="usuario in usuarios | filter:filtrador">
                                <td>{{ usuario.usuario }}</td>
                                <td>{{ usuario.admin }}</td>
                                <td>
                                    <div class="switch">
                                        <label>

                                            <input type="checkbox" ng-checked="{{ usuario.activo }}" disabled />
                                            <span class="lever"></span>

                                        </label>
                                    </div>

                                </td>
                                <td>
                                    <a href="#" class="teat tooltipped waves-effect centrado_vert" data-position="left" data-delay="50" data-tooltip="Configurar a {{ usuario.usuario }}" ng-click="usuario_especifico(usuario.id)">
                                        <i class="material-icons">settings</i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                <div id="modal1" class="modal modal-fixed-footer">
                    <div ng-repeat="data in _usuario">
                        <div class="modal-content">
                            <div class="row">
                                <div class="col s8">
                                    <blockquote>
                                        <h5 class="red-text text-lighten-1 mayuscula">
                                            General
                                        </h5>
                                    </blockquote>
                                </div>
                                <div class="col s4" ng-hide="data.mensaje">
                                   <br>
                                    <div class="switch centrado_vert">
                                        <label>
                                            Inactivo
                                            <input type="checkbox" ng-click="cambiar_activo(data.id)" ng-checked="{{ data.activo }}" id="activo_check" />
                                            <span class="lever"></span>
                                            Activo
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s8">
                                    <input placeholder="Ingrese nombre de usuario" id="usuario" type="text" ng-model="data.usuario">
                                    <label class="active">Nombre de Usuario</label>
                                </div>
                                <div class="input-field col s4 center">
                                    <a class="waves-effect waves-light btn blue tooltipped" data-position="right" data-delay="50" data-tooltip="Comprobar y Cambiar" ng-click="cambiar_nombre_usuario(data.id, data.usuario)">
                                        Cambiar
                                        <i class="material-icons centrado_vert">swap_horiz</i>
                                    </a>
                                </div>
                            </div>

                            <div class="divider"></div>

                            <div class="section">
                                <h6 class="grey-text text-darken-3 mayuscula">
                                    <i class="material-icons centrado_vert">lock</i>
                                    Cambio de Contraseña
                                </h6>
                                <br>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input placeholder="Ingrese Nueva Contraseña" id="c_nueva" type="password" ng-model="clave_nueva">
                                        <label class="active mayuscula">Nueva Contraseña Para {{ data.usuario }}</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input placeholder="Ingrese Contraseña de <?php echo $_SESSION['usuario'] ?>" id="c_actual" type="password" ng-model="clave_actual">
                                        <label class="active mayuscula">Contraseña del usuario actual</label>
                                    </div>
                                    <div class="col s12 center">
                                        <a class="waves-effect waves-light btn grey darken-3" ng-click="cambio_clave_usuario(data.id)">Cambiar</a>
                                    </div>
                                </div>
                            </div>

                            <div class="divider"></div>

                            <!-- Credenciales configuracion -->
                            <blockquote>
                                <h5 class="red-text text-lighten-1 mayuscula">
                                    Credenciales
                                </h5>
                            </blockquote>
                            
                            <div class="progress" ng-hide="ventanas_activas">
                              <div class="indeterminate"></div>
                            </div>
                            
                            <div class="center section" ng-if="data.mensaje">
                                
                                <h4 class="grey-text center text-lighten-2 mayuscula">
                                    Es administrador
                                </h4>
                                <h6 class="grey-text center text-lighten-2 mayuscula">
                                    (No necesita permisos)
                                </h6>
                            </div>

                            <div class="section" ng-hide="data.mensaje">
                                <div  ng-repeat="vent in ventanas">
                                    <table class="bordered highlight mayuscula responsive-table">
                                        <thead>
                                            <th>Acceso a</th>
                                            <th>Permitir</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    inicio
                                                </td>
                                                <td>
                                                    <p>
                                                        <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="inicio" id="check_inicio" ng-checked="{{vent.inicio}}" />
                                                        <label for="check_inicio">Permitido</label>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Clientes
                                                </td>
                                                <td>
                                                    <p>
                                                        <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="clientes" id="check_clientes" ng-checked="{{vent.clientes}}" />
                                                        <label for="check_clientes">Permitido</label>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    vehiculos
                                                </td>
                                                <td>
                                                    <p>
                                                        <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="vehiculos" id="check_vehiculos" ng-checked="{{vent.vehiculos}}" />
                                                        <label for="check_vehiculos">Permitido</label>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Orden de servicio
                                                </td>
                                                <td>
                                                    <p>
                                                        <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="orden" id="check_orden" ng-checked="{{vent.orden}}" />
                                                        <label for="check_orden">Permitido</label>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Inventario
                                                </td>
                                                <td>
                                                    <p>
                                                        <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="inventario" id="check_inventario" ng-checked="{{vent.inventario}}" />
                                                        <label for="check_inventario">Permitido</label>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Configuración
                                                </td>
                                                <td>
                                                    <p>
                                                        <input type="checkbox" class="check_ventanas" ng-click="configurar_ventana($event, vent.id_usuario)" value="configuracion" id="check_configuracion" ng-checked="{{vent.configuracion}}" />
                                                        <label for="check_configuracion">Permitido</label>
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" ng-click="modal_cerrar('modal1')" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
                        </div>
                    </div>
                </div>
                
                <?php
            }
            
            ?>
            
            
            

        </div>

    <?php
}
else
{
    include "page/403.php";
}
?>
