<style>
	#fof{display:block; width:100%; margin:50px auto; line-height:1.6em;}
	#fof .hgroup{text-transform:uppercase;}
	#fof .hgroup h1{margin-bottom:25px; font-size:80px;}
	#fof .hgroup h1 span{display:inline-block; margin-left:5px; padding:2px; border:1px solid #CCCCCC; overflow:hidden;}
	#fof .hgroup h1 span strong{display:inline-block; padding:20px; border:1px solid #CCCCCC; font-weight:normal;}
	#fof .hgroup h2{font-size:60px;}
	#fof .hgroup h2 span{display:block; font-size:30px;}
	#fof p{margin:25px 0 0 0; padding:0; font-size:16px;}
	#fof p:first-child{margin-top:0;}
</style>

<div class="wrapper row2">
  <div id="container" class="clear">
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
    <section id="fof" class="clear center">
      <!-- ####################################################################################################### -->
      <div class="hgroup grey-text">
        <h1>
        	<span><strong>4</strong></span>
        	<span><strong>0</strong></span>
        	<span><strong>4</strong></span>
    	</h1>
        <h2>Error ! <span>Pagina no encontrada</span></h2>
      </div>
      <p class="mayuscula">Parece que esta dirección no existe</p>
      <p><a href="javascript:history.go(-1)">&laquo; Atras</a> / <a href="./">Inicio &raquo;</a></p>
      <!-- ####################################################################################################### -->
    </section>
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
    <!-- ####################################################################################################### -->
  </div>
</div>