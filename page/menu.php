<header>

    <ul id="nav-mobile" class="side-nav hide-on-med-and-down fixed mayuscula" style="overflow: auto; transform: translateX(0px);">

        <li class="logo">

            <?php

            if($_SESSION['logo_empresa'] == '')
            {
                ?>
                    <i class="large material-icons teal-text text-darken-3">work</i>
                <?php
            }
            else
            {
                ?>

                <a id="logo-container" href="#" class="brand-logo">
                    <img src="logos/<?php echo $_SESSION['logo_empresa'] ?>" id="front-page-logo"/>
                </a>
                <?php
            }

            ?>
            <h4 class="teal-text text-darken-3">
                <?php echo $_SESSION['usuario'] ?>
            </h4>
            <h6 class="grey-text">
                <?php

                if($_SESSION['admin'] == 1)
                {
                    echo "administrador";
                }
                else
                {
                    echo "Standard";
                }

                ?>
            </h6>
        </li>


        <?php

        require "php/connect.php";
        $id_actual = $_SESSION['id_usuario'];
        $consultar_activo = "select usuario.activo from usuario where usuario.id = '$id_actual'; ";
        if($answer = $mysql->query($consultar_activo))
        {
            $row_1 = $answer->fetch_assoc();
            $activo_actual = $row_1['activo'];

            if($activo_actual == 0)
            {
                header('location: salir/');
            }

        }
        else
        {
            echo "A ocurrido un error: ". mysqli_error($mysql);
        }


        //Mostrar los elementos del menu segun el administrador
        if($_SESSION['admin'] == 0)
        {

            require "php/paginas.php";


            if($inicio_ == 1)
            {
                ?>
                <li class="bold active" id="inicio">
                    <a href="./?url=inicio" class="waves-effect">
                        <i class="material-icons">home</i>
                        Inicio
                    </a>
                </li>
                <?php
            }
            if($clientes_ == 1)
            {
                ?>
                <li class="bold" id="clientes">
                    <a href="./?url=clientes" class="waves-effect" title="Registro de Clientes">
                        <i class="material-icons">person</i>
                        Clientes
                    </a>
                </li>
                <?php
            }
            if($vehiculos_ == 1)
            {
                ?>
                <li class="bold" id="vehiculos">
                    <a href="./?url=vehiculos" class="waves-effect">
                        <i class="material-icons">folder</i>
                        Vehiculos
                    </a>
                </li>
                <?php
            }
            if($orden_ == 1)
            {
                ?>
                <li class="bold" id="orden">
                    <a href="./?url=orden" class="waves-effect">
                        <i class="material-icons">today</i>
                        Orden de trabajo
                    </a>
                </li>
                <?php
            }
            if($inventario_ == 1)
            {
                ?>
                <li class="bold" id="inventario">
                    <a href="./?url=inventario" class="waves-effect">
                        <i class="material-icons">assignment</i>
                        Inventario
                    </a>
                </li>
                <?php
            }
            if($configuracion_ == 1)
            {
                ?>
                <li class="bold" id="configuracion">
                    <a href="./?url=configuracion" class="waves-effect">
                        <i class="material-icons">settings</i>
                        Configuración
                    </a>
                </li>
                <?php
            }






        }
        else
        {
            ?>
                <li class="bold active" id="inicio">
                    <a href="./?url=inicio" class="waves-effect">
                        <i class="material-icons">home</i>
                        Inicio
                    </a>
                </li>
                <li class="bold" id="clientes">
                    <a href="./?url=clientes" class="waves-effect" title="Registro de Clientes">
                        <i class="material-icons">person</i>
                        Clientes
                    </a>
                </li>
                <li class="bold" id="vehiculos">
                    <a href="./?url=vehiculos" class="waves-effect">
                        <i class="material-icons">folder</i>
                        Vehiculos
                    </a>
                </li>
                <li class="bold" id="orden">
                    <a href="./?url=orden" class="waves-effect">
                        <i class="material-icons">today</i>
                        Orden de trabajo
                    </a>
                </li>
                <li class="bold" id="inventario">
                    <a href="./?url=inventario" class="waves-effect">
                        <i class="material-icons">assignment</i>
                        Inventario
                    </a>
                </li>
                <li class="bold" id="maletin">
                    <a href="./?url=maletin" class="waves-effect">
                        <i class="material-icons">work</i>
                        Maletín
                    </a>
                </li>
                <li class="bold" id="configuracion">
                    <a href="./?url=configuracion" class="waves-effect">
                        <i class="material-icons">settings</i>
                        Configuración
                    </a>
                </li>
            <?php
        }

        ?>


        <li class="bold">
            <a href="salir" class="waves-effect">
                <i class="material-icons">lock</i>
                Cerrar
            </a>
        </li>
    </ul>

    <ul id="slide-out" class="side-nav">
       <br>
        <li class="logo center">

                <?php

                if($_SESSION['logo_empresa'] == '')
                {
                    ?>
                        <i class="large material-icons teal-text text-darken-3">work</i>
                    <?php
                }
                else
                {
                    ?>

                    <a id="logo-container" href="#" class="brand-logo">
                        <img src="logos/<?php echo $_SESSION['logo_empresa'] ?>" id="front-page-logo"/>
                    </a>
                    <?php
                }

                ?>
                <h4 class="teal-text text-darken-3">
                    <?php echo $_SESSION['usuario'] ?>
                </h4>
                <h6 class="grey-text mayuscula">
                    <?php

                    if($_SESSION['admin'] == 1)
                    {
                        echo "administrador";
                    }

                    ?>
                </h6>
            </li>
            <br>
            <li class="bold active" id="inicio">
                <a href="./?url=inicio" class="waves-effect">
                    <i class="material-icons">home</i>
                    Inicio
                </a>
            </li>
            <li class="bold" id="clientes">
                <a href="./?url=clientes" class="waves-effect" title="Registro de Clientes">
                    <i class="material-icons">person</i>
                    Clientes
                </a>
            </li>
            <li class="bold" id="vehiculos">
                <a href="./?url=vehiculos" class="waves-effect">
                    <i class="material-icons">folder</i>
                    Vehiculos
                </a>
            </li>
            <li class="bold">
                <a href="#" class="waves-effect">
                    <i class="material-icons">settings</i>
                    Servicios
                </a>
            </li>
            <li class="bold">
                <a href="./?url=inventario" class="waves-effect">
                    <i class="material-icons">assignment</i>
                    Inventario
                </a>
            </li>
            <li class="bold">
                <a href="salir" class="waves-effect">
                    <i class="material-icons">lock</i>
                    Cerrar
                </a>
            </li>
      </ul>

</header>

<main class="gray darken-2 main-margin">
    <div class="section teal darken-3" id="index-banner">
        <div class="container">
            <div class="row">
                <div class="col s8 m12 l8 center-on-med-only">
                   <a data-activates="nav-mobile" class="button-collapse top-nav waves-effect waves-light circle hide-on-large-only">
                        <i class="material-icons">menu</i>
                    </a>
                    <h4 class="light teal-text text-lighten-5" id="titulo">
                        {{ titulo | uppercase }}
                    </h4>
                </div>
                <div class="col s4 l4 hide-on-med-only" ng-hide="hide_search">
                    <div class="input-field" ng-show="titulo">
                        <input id="search" class="mayuscula" type="search" required placeholder="bucar en {{ titulo }}" ng-model="filtrador">
                        <i class="material-icons" style="margin-top: 8px;" ng-click="filtrador='';">close</i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php

    if(@$_GET['url'])
    {
        $url = $_GET['url'];
        if(@include $url.".php")
        {
            /*if(stream)
            {
                echo 'error';
            }*/
            ?>

            <script type="text/javascript">
                $('.bold').removeClass('active');
                $('#<?php echo $url; ?>').addClass('active');
            </script>

            <?php

        }
        else
        {
            include "page/404.php";
        }


    }
    else
    {
        include "page/inicio.php";
    }


    ?>
</main>
