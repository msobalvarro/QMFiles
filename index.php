<!DOCTYPE html>
<html lang="en" ng-app="qm">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>QM files</title>
    <link rel="stylesheet" type="text/css" href="css/ghpages-materialize.css" />
	<link rel="stylesheet" type="text/css" href="css/sweetalert.css" />
    <link rel="stylesheet" type="text/css" href="css/master.css" />
    <script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
    <script type="text/javascript" src="js/angular.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/sweetalert.min.js"></script>
</head>
<body>

    <div ng-controller="controlador" ng-init="plugin()">
        <?php
            @session_start();
            if(isset($_SESSION['usuario']))
            {
                if($_SESSION['vez'] == 0)
                {
                    include "page/bienvenida.php";
                }
                else
                {
                    include "page/menu.php";
                }

                #include "page/footer.php";
            }
            else
            {
                if(isset($_SESSION['try']))
                {
                    include "page/try.php";
                    unset($_SESSION['try']);
                }
                else
                {
                    echo include "page/login.php";
                }
            }
        ?>
    </div>
   <?php

    #nclude "page/footer.php";

    ?>


<script type="text/javascript" src="js/sweetalert-dev.js"></script>
<script type="text/javascript" src="js/master.js"></script>
<script type="text/javascript" src="js/plugin.js"></script>
</body>
</html>
