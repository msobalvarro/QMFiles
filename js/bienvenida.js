$(function(){
    setTimeout(function(){
        $('.capa-1').fadeToggle();
    }, 3000);
    $('.btn-capa-2').click(desaparecer2);
    $('.btn-capa-3').click(desaparecer3);
    
    $('.atras').click(function(){
        $('.capa-2').fadeIn();
        $('.capa-1').fadeOut();
    });
    
    function desaparecer1()
    {
        
        $('.capa-1').fadeToggle();
    }
    function desaparecer2()
    {
        if($("#nombre_empresa").val() == "")
        {
            swal({
                  title: "Agrege un Nombre <span class='glyphicon glyphicon-pencil'></span>",
                  timer: 2000,
                  showConfirmButton: true, 
                    html: true
                });   
        }
        else
        {            
            $('.capa-2').fadeToggle();
        }
    }
    function desaparecer3()
    {
        
        $('.capa-3').fadeToggle();
    }
})