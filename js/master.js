var app = angular.module("qm", []);
app.controller("controlador", ['$scope', '$http', '$filter', function ($scope, $http, $filter) {

    var json_clientes,
        id_client;
    $http.get('http://ipinfo.io').success(function (data) {
        $scope.ip_local = data.ip
    })

    var filtroDate = $filter("date");
    $scope.hora = filtroDate(new Date(), "h:mma");
    $scope.fecha = filtroDate(new Date(), "dd/MM/yyyy");


    $scope.portapapeles
    $scope.ProductoListo = true


    if (localStorage.getItem('ItemShop')) {
        $scope.productos_compra = JSON.parse(localStorage.getItem('ItemShop'))
        $scope.muestra_shop = true
    } else {
        $scope.productos_compra = []
        $scope.muestra_shop = false
    }

    $scope.alto_dispositivo = function (argument) {
        var alto = $(window).height();
        return alto;
    }

    $scope.getIp = function () {
        return $scope.ip_local
    }

    $scope.login = function () {
        $scope.cargar = true;
        $http.get('http://ipinfo.io').success(function (data) {
            $http.post('json/verificar_login.php', {
                usuario: $scope.usuario,
                clave: $scope.clave,
                ip: data.ip
            }).success(function (data) {

                if (data == 1) {
                    $('#login').submit();
                } else {
                    $scope.login_incorrecto = true;
                    $scope.toast(data);
                }
                $scope.cargar = false;
            });
        })
    }

    $scope.traer_logos = function () {
        $http.get('json/logos.php').success(function (data) {
            $scope.logos_empresa = data;
            console.log(data);
        });
    }

    $scope.modal = function (target) {
        $('#' + target).modal('open');
    }

    $scope.modal_cerrar = function (target) {
        $('#' + target).modal('close');
    }

    $scope.traer_clientes = function () {
        $http.get('json/clientes.php').success(function (data) {
            $scope.clientes = data;
        });
    }

    $scope.traer_vehiculo = function () {
        $http.get('json/vehiculos.php').success(function (data) {
            $scope.vehiculos = data;
        });
    }

    $scope.traer_inventario = function () {
        $http.get('json/inventario.php').success(function (data) {
            $scope.inventario = data;

            for (var za = 0; za < $scope.inventario.length; za++) {
                for (var cs = 0; cs < $scope.productos_compra.length; cs++) {
                    if ($scope.inventario[za].id == $scope.productos_compra[cs].id) {
                        $scope.inventario[za].activo = false
                    }
                }
            }

        });
    }

    $scope.traer_repuestos_ot = function (id) {
        $http.get('json/repuestos_ot.php?id=' + id).success(function (data) {
            $scope.repuesto_ot = data;
        });
    }
    /*var limite = 10;

    $(window).scroll(function(){
        if($(window).scrollTop() == $(document).height() - $(window).height())
        {
            limite = limite+10;
            $scope.load_more = $filter(limite)($scope.inventario);
            //$scope.toast($scope.load_more);
        }
    });
    */
    $scope.traer_ot = function () {
        $http.get('json/ot.php').success(function (data) {
            $scope.ot = data;
        });
    }

    $scope.aumentar_limit = function () {

    }

    $scope.toast = function (texto) {
        Materialize.toast(texto, 3000, 'rounded');
    }

    $scope.add_cliente = function () {
        var tipo_id = $('#client_tipo_id').val();
        var tipo_cliente = $('#client_tipo').val();

        var envio = $http.post("json/nuevo_cliente.php", {
            nombre: $scope.cliente_nombre,
            tipo_cliente: tipo_cliente,
            identificacion: $scope.cliente_identificacion,
            tipo_id: tipo_id,
            telefono: $scope.cliente_telefono,
            direccion: $scope.cliente_direccion,
            email: $scope.cliente_email,
            emergencia: $scope.cliente_emergencia
        });

        envio.success(function (data) {
            $scope.traer_clientes();
            //$('#modal1').modal('close');

            //Vaciar los campos de la modal
            $scope.cliente_nombre = "";
            $scope.cliente_identificacion = "";
            $scope.cliente_telefono = "";
            $scope.cliente_direccion = "";
            $scope.cliente_email = "";
            $scope.cliente_emergencia = "";

            swal(data);


        });


    }

    $scope.edit_cliente = function (id, modo) {

    }

    $scope.borrar_usuario = function (id_cliente, nombre_cliente) {
        swal({
                title: "Seguro que quiere eliminar a " + nombre_cliente,
                text: "Eliminando a este cliente desaparecera todo su registro",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00695c",
                confirmButtonText: "Si, eliminar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            },
            function () {
                $http.post('json/eliminar_cliente.php', {
                    id: id_cliente,
                    nombre: nombre_cliente
                }).success(function (data) {
                    swal.close();
                    $scope.traer_clientes();
                    $scope.toast('Cliente Archivado');
                });
            });
    }

    $scope.borrar_vehiculo = function (id_vehiculo, marca_vehiculo) {
        swal({
                title: "Seguro que quiere eliminar a " + marca_vehiculo,
                text: "Eliminando a este vehiculo desaparecera todo su registro",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00695c",
                confirmButtonText: "Si, eliminar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            },
            function () {
                $http.post('json/eliminar_vehiculo.php', {
                    id: id_vehiculo,
                    nombre: marca_vehiculo
                }).success(function (data) {
                    swal.close();
                    $scope.traer_vehiculo();
                    $scope.cliente_vehiculo_especifico($scope.id_cliente_seleccionado_vehiculo);
                    $scope.toast('Vehiculo Archivado');
                });
            });
    }

    $scope.dato_cliente_especifico = function (id_client) {
        $http.get('json/cliente_especifico.php?id=' + id_client).success(function (data) {

            $scope.cliente_especifico = data;
            $scope.modal('modal3');
            //$scope.toast(data);

        });
    }

    $scope.dato_vehiculo_especifico = function (id_vehiculo, dx) {
        $http.get('json/vehiculo_especifico.php?vh=' + id_vehiculo).success(function (data) {

            $scope.vehiculo_especifico = data;
            $scope.modal('modal3');
            $scope.modal('modal30');
            //$scope.toast(data);

        });
    }

    $scope.dato_vehiculo_especifico_cliente = function (id_vehiculo) {
        $http.get('json/vehiculo_especifico.php?vh=' + id_vehiculo).success(function (data) {

            $scope.vehiculo_especifico = data;
            $scope.modal('modal30');
            //$scope.toast(data);

        });
    }

    $scope.cliente_vehiculo_especifico = function (id_usuario) {
        $scope.id_cliente_seleccionado_vehiculo = id_usuario;
        $http.get('json/vehiculos_cliente_especifico.php?cliente=' + id_usuario).success(function (data) {

            $scope.dato_vehiculo = data;
            //$scope.toast(data);

        });
    }

    $scope.cliente_seleccion = function (nombre, id_seleccion) {
        $scope.cliente_seleccionado = nombre;
        $scope.id_cliente_seleccionado = id_seleccion;
        $scope.modal_cerrar('modal3');
        $scope.toast('Cliente seleccionado');
    }

    $scope.add_vehiculo = function () {
        $http.post('json/nuevo_vehiculo.php', {
            color: $scope.vehiculo_color,
            placa: $scope.vehiculo_placa,
            marca: $scope.vehiculo_marca,
            modelo: $scope.vehiculo_modelo,
            motor: $scope.vehiculo_motor,
            chasis: $scope.vehiculo_chasis,
            recorrido: $scope.vehiculo_recorrido,
            cilindros: $('#cilindros').val(),
            transmicion: $('#transmicion').val(),
            tipo_motor: $('#tipo_motor').val(),
            uso: $('#tipo_uso').val(),
            cliente: $scope.id_cliente_seleccionado,

        }).success(function (data) {
            $scope.traer_vehiculo();
            $scope.modal_cerrar('modal1');

            swal(data);
        });
    }

    $scope.modific_cliente = function (id_especifico) {
        $scope.modal_cerrar('modal3');
        $http.get('json/cliente_especifico.php?id=' + id_especifico).success(function (data) {
            $scope.modal('modal2');
            $scope.datos_clientes = data;
        });
    }

    $scope.editar_cliente = function (cliente) {
        var nombre_edit = $('#nombre_edit').val(),
            telefono_edit = $('#telefono_edit').val(),
            identificacion_edit = $('#identificacion_edit').val(),
            emergencia_edit = $('#emergencia_edit').val(),
            direccion_edit = $('#direccion_edit').val(),
            email_edit = $('#email_edit').val();

        $http.post('json/editar_cliente.php', {
            nombre: nombre_edit,
            telefono: telefono_edit,
            identificacion: identificacion_edit,
            emergencia: emergencia_edit,
            direccion: direccion_edit,
            email: email_edit,
            id_cliente: cliente
        }).success(function (message) {

            $scope.traer_clientes();
            $scope.modal_cerrar('modal2');
            swal(message);

        });

    }

    $scope.agregar_registro = function (x, md, mc) {
        var id_cliente;

        if (x == 0) {
            var tipo_id = $('#client_tipo_id').val();
            var tipo_cliente = $('#client_tipo').val();

            var envio = $http.post("json/nuevo_registro.php", {
                accion: x,
                nombre: $scope.cliente_nombre,
                tipo_cliente: tipo_cliente,
                identificacion: $scope.cliente_identificacion,
                tipo_id: tipo_id,
                telefono: $scope.cliente_telefono,
                direccion: $scope.cliente_direccion,
                email: $scope.cliente_email,
                emergencia: $scope.cliente_emergencia
            }).success(function (repuesta) {
                console.log(repuesta)
                if (repuesta == 0) {
                    swal("Error al registrar cliente");
                }
                if (repuesta == 1) {
                    swal("Rellene todos los campos");
                }
                if (repuesta != 0 & repuesta != 1) {
                    swal({
                            title: "Registro Agregado Correctamente",
                            text: "Agrega un vehiculo para este cliente. Agregar Vehiculo?",
                            showCancelButton: true,
                            confirmButtonText: "Si, Agregar Vehiculo",
                            cancelButtonText: "No, Gracias",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                swal("Cliente Agregado", "", "success");
                                if (md && mc) {
                                    $scope.modal_cerrar(md)
                                    $scope.modal(mc)
                                }
                                $scope.modal_cerrar('modal1');
                                $scope.modal('modal4');
                            } else {
                                $scope.modal_cerrar('modal1');
                                swal("Cliente Agregado", "", "success");
                            }

                        });
                    if (!md) {
                        $scope.traer_clientes()
                    }
                }
            });
        }

        if (x == 1) {
            $http.post('json/nuevo_registro.php', {
                accion: x,
                cliente: id_cliente,
                color: $scope.vehiculo_color,
                placa: $scope.vehiculo_placa,
                marca: $scope.vehiculo_marca,
                modelo: $scope.vehiculo_modelo,
                motor: $scope.vehiculo_motor,
                chasis: $scope.vehiculo_chasis,
                recorrido: $scope.vehiculo_recorrido,
                cilindros: $('#cilindros').val(),
                transmicion: $('#transmicion').val(),
                tipo_motor: $('#tipo_motor').val(),
                uso: $('#tipo_uso').val(),
                cliente: id_cliente,

            }).success(function (repuesta) {
                if (repuesta == 0) {
                    swal("Error al registrar vehiculo");
                }
                if (repuesta == 1) {
                    swal({
                            title: "Desea Descartar Cambios?",
                            text: "Rellene todos los campos para continuar, de manera contraria los registros del cliente se guardarán",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Si, Descartar",
                            cancelButtonText: "Cancelar",
                            closeOnConfirm: false
                        },
                        function () {

                            swal("Registro cancelado", "Los datos personales del cliente se registraron", "error");
                            $scope.modal_cerrar('modal4');

                        });

                }
                if (repuesta == 2) {
                    swal("Vehiculo Registrado", "Ok para continuar", "success");
                    $scope.modal_cerrar('modal4');
                    $scope.traer_clientes();
                }
            });

        }
    }

    $scope.add_inventario = function () {
        $http.post('json/nuevo_inventario.php', {
            nombre: $scope.nombre,
            codigo: $scope.codigo,
            precio: $scope.precio,
            existencia: $scope.cantidad,
            descripcion: $scope.descripcion

        }).success(function (data) {
            $scope.traer_inventario();

            swal(data);
        });
    }
    //***********************************
    $scope.add_compra = function (producto, id, index) {

        var productoActual = {
            'id': $scope.inventario[index].id,
            'descripcion': $scope.inventario[index].descripcion,
            'codigo': $scope.inventario[index].codigo,
            'nombre': $scope.inventario[index].nombre,
            'existencia': $scope.inventario[index].existencia,
            'precio': $scope.inventario[index].precio,
            'unidad': 1
        }

        $scope.inventario[index].activo = false

        $scope.productos_compra.unshift(productoActual)
        console.log()
        //$('#p_' + id).fadeOut(100)
    }

    $scope.eliminarItemsShop = function () {


        for (var i = 0; i < $scope.inventario.length; i++) {
            $scope.inventario[i].activo = true
        }

        $scope.productos_compra = []
        localStorage.clear()

    }
    //***********************************

    $scope.eliminar_elemento = function (index, id) {
        if (id) {
            for (var zs = 0; zs < $scope.productos_compra.length; zs++) {
                if ($scope.productos_compra[zs].id == id) {
                    $scope.productos_compra.splice(zs, 1)
                }
            }
        } else {
            $scope.productos_compra.splice(index, 1)
        }
    }

    $scope.comprobar_datos_carro = function () {
        if ($scope.productos_compra.length == 0) {
            $scope.existencia_inv = false;
        } else {
            $scope.existencia_inv = true;
        }
    }

    $scope.total_compra = function () {
        $scope.total = 0

        for (item of $scope.productos_compra) {
            $scope.total += Number(item.precio);
        }
        return Number($scope.total);

    }

    $scope.total_general = function () {
        $scope.total_g = 0
        for (var z = 0; z < $scope.productos_compra.length; z++) {
            $scope.total_g += Number($scope.productos_compra[z].precio * $scope.productos_compra[z].unidad)
        }

        if ($scope.iva_fac) {
            $scope.total_g = Number(($scope.total_g * 0.15) + $scope.total_g)
        }

    }

    $scope.eliminar_inventario = function (_id, _nombre) {
        swal({
                title: "Seguro que quiere eliminar a " + _nombre,
                text: "Se eliminará todo registro de este producto",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, eliminar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            },
            function () {
                $http.post('json/eliminar_inventario.php', {
                    id: _id,
                    nombre: _nombre
                }).success(function (data) {
                    swal("Eliminado", data, "success");
                    $scope.traer_inventario();
                    $scope.toast('Producto Elimado');
                });
            });
    }

    $scope.facturar_carrito = function () {
        if ($scope.productos_compra.length == 0) {
            swal('No hay registros que facturar', 'Agregue algún Registro', 'warning');
        } else {
            localStorage.setItem('ItemShop', JSON.stringify($scope.productos_compra))
            window.location = './?url=carrito'
        }
    }

    $scope.facturar_compra = function () {
        swal({
                title: "Facturar Compra?",
                text: "Al facturar esta compra se registrará en tu maletin de documentos",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#c3a036",
                confirmButtonText: "Si, Facturar!",
                closeOnConfirm: false
            },
            function () {
                $http.post('json/facturar_compra.php', {
                    datos: $scope.productos_compra,
                    iva: $scope.iva_fac
                }).success(function (data) {
                    swal(data.message, ' ', 'success')
                    localStorage.clear()
                    $scope.btn_buy = true

                    $('#SECT_HIDE_PDF').slideUp()
                    $('#PDF_FACT').slideDown()

                    $('#PDF_FACT').append('<embed name="plugin" id="plugin" id="Embed_PDF_fact" src="http://localhost/qm/facturar/?data=' + data.json + ' " type="application/pdf" internalinstanceid="10">')
                })
            })

    }

    $scope.traer_usuarios = function () {
        $http.get('json/usuarios.php').success(function (data) {
            $scope.usuarios = data;
        });
    }

    $scope.usuario_especifico = function (id) {
        $scope.id_usuario_especifico = id;
        $http.get('json/usuario_especifico.php?id=' + id).success(function (data) {
            $scope._usuario = data;
            $scope.modal('modal1');
            $scope.ventanas_activas(id);
        });
    }

    $scope.cambiar_activo = function (id) {
        var activo;
        if ($('#activo_check').is(':checked')) {
            activo = 1;

        } else {
            activo = 0;
        }

        $http.post('json/configurar_usuario.php', {
            accion: 1,
            activo: activo,
            id_usuario: id
        }).success(function (answer) {
            swal(answer, '', 'success');
            $scope.traer_usuarios();
        });
    }

    $scope.cambiar_nombre_usuario = function (id, usuario) {
        $http.post('json/configurar_usuario.php', {
            accion: 2,
            nombre: usuario,
            id_usuario: id
        }).success(function (answer) {
            swal(answer, 'Para ver los cambios volver a iniciar sesion', 'success');
            $scope.traer_usuarios();
        });
    }

    $scope.cambio_clave_usuario = function (id) {
        var clave_actual = $('#c_actual').val(),
            clave_nueva = $('#c_nueva').val();

        $http.post('json/configurar_usuario.php', {
            accion: 3,
            ca: clave_actual,
            cn: clave_nueva,
            id_usuario: id
        }).success(function (answer) {
            swal(answer);
            $scope.traer_usuarios();
            $('#c_actual').val("");
            $('#c_nueva').val("");
        });
    }

    $scope.ventanas_activas = function (id) {
        $http.get('json/ventanas_activas.php?id=' + id).success(function (request) {
            $scope.ventanas = request;
            //swal(request);
        });
    }

    $scope.configurar_ventana = function (data, id) {
        var id_usuario = id,
            id = '#' + data.currentTarget.id,
            target = $(id).val(),
            valor;


        if ($(id).is(':checked')) {
            valor = 1;
        } else {
            valor = 0;
        }

        $http.post('json/configurar_ventana.php', {
            objetivo: target,
            val: valor,
            usuario: id_usuario
        }).success(function (data) {
            swal(data);
            $scope.traer_usuarios();
        });
    }

    $scope.edit_vehiculo = function (id) {
        $http.get('json/vehiculo_especifico.php?vh=' + id).success(function (data) {
            $scope.vehiculo_especifico = data;
            $scope.modal('modal5');
        });
    }

    $scope.editar_vehiculo = function (id) {
        var color_v = $('#vh_color').val(),
            placa_v = $('#vh_placa').val(),
            marca_v = $('#vh_marca').val(),
            modelo_v = $('#vh_modelo').val(),
            motor_v = $('#vh_motor').val(),
            chasis_v = $('#vh_chasis').val(),
            recorrido_v = $('#vh_recorrido').val();

        $http.post('json/editar_vehiculo.php', {
            placa: placa_v,
            color: color_v,
            marca: marca_v,
            modelo: modelo_v,
            motor: motor_v,
            chasis: chasis_v,
            recorrido: recorrido_v,
            id_vehiculo: id
        }).success(function (data) {
            swal(data);
            $scope.modal_cerrar('modal5');
            $scope.traer_vehiculo();
        });
    }

    $scope.vehiculos_orden = function () {
        $http.get('json/vehiculos_orden.php').success(function (datos) {
            $scope.vehiculos_ot = datos;
        });
    }

    $scope.configurar_ot = function (accion, id_vehiculo, Mo, Mc) {
        if (accion == 1) {
            id_client = id_vehiculo;
            $scope.modal_cerrar('modal1');
            $http.get('json/vehiculo_especifico.php?vh=' + id_vehiculo).success(function (data) {
                $scope.modal('modal2');
                if (Mo && Mc) {
                    $scope.modal(Mo)
                    $scope.modal_cerrar(Mc)
                }
                $scope.vehiculo_ot = data;
            });
        } else {
            var tipo_servicio = $('#tipo_servicio').val(),
                area_afectada = $('#area_afectada').val(),
                tipo_trabajo = $('#tipo_trabajo').val();

            $http.post('json/nueva_ot.php', {
                id_vehiculo: id_client,
                tipo_servicio: tipo_servicio,
                area_afectada: String(area_afectada),
                tipo_trabajo: String(tipo_trabajo),
                recorrido: $scope._recorrido,
                costo: $scope.costo_ot
            }).success(function (respuesta) {
                swal(respuesta);
                $scope.traer_ot();
                $scope.modal_cerrar('modal2');
                if (Mo) {
                    $scope.modal_cerrar(Mo)
                }
            });

        }
    }

    $scope.ot_especifica = function (_id) {
        if (_id != true) {
            $scope.id_ot_especifica = _id;
            $http.post('json/ot_especifica.php', {id: $scope.id_ot_especifica}).success(function (data) {
                $scope.dato_ot = data;
                $scope.traer_repuestos_ot(_id);
                $scope.modal('modal3');
            }).error(function (data) {
                swal(data, "Se a Encontrado un error", "error");
            });

            $scope.toast($scope.id_ot_especifica);
        } else {
            $scope.modal('modal3');
        }

    }

    $scope.abrir_inventario_ot = function (id) {
        $scope.ot_id_seleccion = id;
        $scope.toast($scope.ot_id_seleccion);
        $scope.modal_cerrar('modal3');
        $scope.modal('modal4');
        $scope.traer_inventario();
    }

    $scope.add_repuesto = function (inventario) {
        //$scope.toast(inventario);
        $scope.toast($scope.ot_id_seleccion);
        /*
        $http.post('json/add_repuesto_ot.php', {
            id_ot: $scope.ot_id_seleccion,
            cantidad: 1,
            id_inventario: inventario
        }).success(function(data){
            $scope.toast(data);
        });
        */
    }

    $scope.eliminar_repuesto = function (id_repuesto, nombre_repuesto, id_ot) {
        swal({  
                title: "Eliminar Repuesto",
                  text: "Desea eliminar repuesto de la orden de trabajo?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#00695c",
                  confirmButtonText: "Si, Eliminar",
                  cancelmButtonText: "Cancelar",
                  closeOnConfirm: false
            },
            function () {
                $http.post('json/eliminar_repuesto_ot.php', {
                    id: id_repuesto,
                    nombre: nombre_repuesto
                }).success(function (repuesta) {
                    swal(repuesta, "", "success");
                    $scope.traer_repuestos_ot(id_ot);
                });
            });
    }

    $scope.cerrar_orden = function (id_ot) {
        swal({  
                title: "Cerrar Orden",
                  text: "Desea Cerrar la Orden de Trabajo?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#607d8b",
                  confirmButtonText: "Si, Cerrar Orden",
                  cancelButtonText: "No, Cancelar",
                  closeOnConfirm: false
            },
            function () {  
                $http.post('json/cerrar_orden.php', {
                    id: id_ot
                }).success(function (data) {
                    swal.close();
                    $scope.toast(data);
                    $scope.traer_ot();
                });
            });
    }

    $scope.abrir_orden = function (id_ot) {
        swal({  
                title: "Abrir Orden",
                  text: "Desea Abiri la Orden de Trabajo?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#5d4037",
                  confirmButtonText: "Si, Abrir Orden",
                  cancelButtonText: "No, Cancelar",
                  closeOnConfirm: false
            },
            function () {  
                $http.post('json/abrir_orden.php', {
                    id: id_ot
                }).success(function (data) {
                    swal.close();
                    $scope.toast(data);
                    $scope.traer_ot();
                });
            });
    }

    $scope.eliminar_orden = function (id_ot) {
        swal({  
                title: "Eliminar Orden",
                  text: "Desea Eliminar la Orden de Trabajo?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Si, Eliminar",
                  cancelButtonText: "Cancelar",
                  closeOnConfirm: false
            },
            function () {  
                $http.post('json/eliminar_ot.php', {
                    id: id_ot
                }).success(function (data) {
                    swal.close();
                    $scope.toast(data);
                    $scope.traer_ot();
                });
            });
    }

    $scope.grafica_ot = function () {

        $http.get('json/datos_grafica_ot.php').success(function (data) {
            $scope.valores = [];
            $scope.nombres = [];

            angular.forEach(data, function (argument) {

                $scope.valores.push(argument.value);
                $scope.nombres.push(argument.text);
            })

            $scope.myJson = {
                globals: {
                    shadow: false,
                    fontFamily: "segoe ui",
                    fontWeight: "100"
                },
                type: "pie",
                backgroundColor: "#fff",

                legend: {
                    position: "0%",
                    borderColor: "none",
                    marker: {
                        borderRadius: 50,
                        borderColor: "transparent"
                    }
                },
                tooltip: {
                    text: "%t"
                },
                plot: {
                    refAngle: "-90",
                    borderWidth: "0px",
                    valueBox: {
                        placement: "in",
                        text: "%npv %",
                        fontSize: "15px",
                        textAlpha: 1,
                    }
                },
                series: data
            };



        });

    }

    $scope.filtar_grafica_ot = function () {
        var f_f = $('date_end').val(),
            f_i = $('date_begin').val();

        if ($scope.date_begin == null | $scope.date_end == null) {
            swal("Ingrese los dos campos", "Ingrese fecha Inicial y fecha Final para poder ejecutar esta acción", "warning");
        } else {
            $http.post('json/filtrar_grafica_ot.php', {
                    fecha_inicio: $scope.date_begin,
                    fecha_fin: $scope.date_end
                })
                .success(function (data) {
                    $scope.myJson = {
                        globals: {
                            shadow: false,
                            fontFamily: "segoe ui",
                            fontWeight: "100"
                        },
                        type: "pie",
                        backgroundColor: "#fff",

                        legend: {
                            position: "0%",
                            borderColor: "none",
                            marker: {
                                borderRadius: 50,
                                borderColor: "transparent"
                            }
                        },
                        tooltip: {
                            text: "%t"
                        },
                        plot: {
                            refAngle: "-90",
                            borderWidth: "0px",
                            valueBox: {
                                placement: "in",
                                text: "%npv %",
                                fontSize: "15px",
                                textAlpha: 1,
                            }
                        },
                        series: data
                    };
                });
        }

    }

    $scope.generate_bill = function () {
        $scope.preloader_bill = true
        $http.get('json/generate_base.php?data='+$scope.num_bill).success(function(data){
            $('#responsiveBil').html('<embed name="plugin" id="plugin" src="http://localhost/qm/facturar/?data=' + data + ' " type="application/pdf" internalinstanceid="10">')
        })

    }
}]);
