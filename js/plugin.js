$(function () {
    $('select').material_select()
    $(".button-collapse").sideNav()
    $('.modal').modal()
    $('.collapsible').collapsible()
    $('.carousel').carousel()
    $('ul.tabs').tabs()
    $('.materialboxed').materialbox()
    $('.carousel').carousel({
        fullWidth: true
    })
    $('.carousel.carousel-slider').carousel({
        fullWidth: true
    })
    $('.datepicker').pickadate()
    $('.scrollspy').scrollSpy()

});
