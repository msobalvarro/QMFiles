<?php
require "../php/connect.php";

$id_orden = $_GET['id'];

$sql = "select repuesto.*, inventario.nombre as nombre_repuesto, inventario.codigo as codigo_repuesto, inventario.descripcion as descripcion_repuesto, inventario.precio as precio_repuesto from repuesto, inventario where inventario.id = repuesto.id_inventario and repuesto.ot = '$id_orden' ";

$datos = array();
if($consulta = $mysql->query($sql))
{
    while($fila = $consulta->fetch_assoc())
    {
        $id_repuesto = $fila['id'];
        $ot = $fila['ot'];
        $id_inventario = $fila['id_inventario'];
        $cantidad = $fila['cantidad'];
        $nombre_repuesto = $fila['nombre_repuesto'];
        $codigo_repuesto = $fila['codigo_repuesto'];
        $descripcion_repuesto = $fila['descripcion_repuesto'];
        $precio_repuesto = $fila['precio_repuesto'];
        
        $datos[] = array('id'=>$id_repuesto, 'id_inventario'=>$id_inventario, 'cantidad'=>$cantidad, 'repuesto'=>$nombre_repuesto, 'codigo'=>$codigo_repuesto, 'descripcion'=>$descripcion_repuesto, 'precio'=>$precio_repuesto, 'id_ot'=>$ot);
    }
    $json = json_encode($datos);
}
echo $json;