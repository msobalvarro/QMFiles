<?php
require "../php/connect.php";

@session_start();
$empresa = $_SESSION['id_empresa'];
$id_usuario = $_SESSION['id_usuario'];

$sql = "
    select ot.*, vehiculo.marca as marca_vehiculo, vehiculo.modelo as modelo_vehiculo, vehiculo.placa as placa_vehiculo, vehiculo.id_cliente, cliente.nombres as nombre_cliente, cliente.tipo_cliente, cliente.identificacion as identificacion_cliente from ot, vehiculo, cliente where ot.empresa = '$empresa' and vehiculo.id_cliente = cliente.id and ot.id_vehiculo = vehiculo.id and ot.activa = 1 and vehiculo.activo = 1 order by ot.id DESC; ";

/*
select * from ot left join fecha_cierre as fc on ot.fecha_c = fc.id;
*/

$datos = array();
if($consulta = $mysql->query($sql))
{
    while($fila = $consulta->fetch_assoc())
    {
        $id_ot = $fila['id'];
        $id_vehiculo = $fila['id_vehiculo'];
        $id_cliente = $fila['id_cliente'];
        $nombre_cliente = $fila['nombre_cliente'];
        $tipo_cliente = $fila['tipo_cliente'];
        $identificacion_cliente = $fila['identificacion_cliente'];
        $tipo_servicio = $fila['tipo_servicio'];
        $area_afectada = $fila['area_afectada'];
        $tipo_trabajo = $fila['tipo_trabajo'];
        $costo = $fila['costo'];
        $abierta = $fila['abierta'];
        $usuario = $fila['usuario'];
        $marca_vehiculo = $fila['marca_vehiculo'];
        $modelo_vehiculo = $fila['modelo_vehiculo'];   
        $fecha_abierta = $fila['fecha_a'];
        $fecha_cierre = $fila['fecha_c'];

        if($abierta == 0)
        {
            $abierta = null;
        }
        
        $datos[] = array('id_ot'=>$id_ot, 
                         'id_vehiculo'=>$id_vehiculo,
                         'id_cliente' =>$id_cliente,
                         'nombre_cliente' =>$nombre_cliente,
                         'tipo_cliente' =>$tipo_cliente,
                         'identificacion_cliente' =>$identificacion_cliente,
                         'tipo_servicio' =>$tipo_servicio,
                         'area_afectada' =>$area_afectada,
                         'tipo_trabajo' =>$tipo_trabajo,
                         'costo' =>$costo,
                         'abierta' =>$abierta,
                         'usuario' =>$usuario,
                         'marca_vehiculo' =>$marca_vehiculo,
                         'modelo_vehiculo' =>$modelo_vehiculo,
                         'fecha'=>$fecha_abierta,
                         'fecha_cierre'=>$fecha_cierre
                        );
    }
    $json = json_encode($datos);
    echo $json;
}
else
{
    echo "A ocurrido un error: ".mysqli_error($mysql);
}
