<?php
require "../php/connect.php";

$postdata = file_get_contents("php://input", true);
$request = json_decode($postdata);

if($id = $request->id)
{
    $sql = "select ot.*, vehiculo.marca as marca_vehiculo, vehiculo.modelo as modelo_vehiculo, 
    vehiculo.placa as placa_vehiculo, vehiculo.id_cliente, cliente.nombres as nombre_cliente, 
    cliente.tipo_cliente, cliente.identificacion as identificacion_cliente from ot, vehiculo, cliente 
    where ot.id_vehiculo = vehiculo.id and vehiculo.id_cliente = cliente.id 
    and ot.activa = 1 and ot.id = '$id' ;";

    /*$sql = "
    select ot.*, vehiculo.marca as marca_vehiculo, vehiculo.modelo as modelo_vehiculo, 
    vehiculo.placa as placa_vehiculo, vehiculo.id_cliente, cliente.nombres as nombre_cliente, 
    cliente.tipo_cliente, cliente.identificacion as identificacion_cliente, 
    hora.dia, hora.mes, hora.ano, hora.hora, hora.minutos, hora.mp, 
    fecha_cierre.dia as dia_cierre, fecha_cierre.mes as mes_cierre, fecha_cierre.ano as ano_cierre, 
    fecha_cierre.hora as hora_cierre, fecha_cierre.minutos as minutos_cierre, fecha_cierre.mp as mp_cierre
    from ot, vehiculo, cliente, hora, fecha_cierre where ot.empresa = '$empresa' 
    and vehiculo.id_cliente = cliente.id and ot.id_vehiculo = vehiculo.id and  
    hora.id = ot.fecha_a and ot.activa = 1;";*/

    $datos = array();
    if($consulta = $mysql->query($sql))
    {
        $fila = $consulta->fetch_assoc();
        $id_ot = $fila['id'];
        $id_vehiculo = $fila['id_vehiculo'];
        $id_cliente = $fila['id_cliente'];
        $nombrecliente = $fila['nombre_cliente'];
        $tipo_cliente = $fila['tipo_cliente'];
        $identificacion_cliente = $fila['identificacion_cliente'];
        $tipo_servicio = $fila['tipo_servicio'];
        $area_afectada = $fila['area_afectada'];
        $tipo_trabajo = $fila['tipo_trabajo'];
        $costo = $fila['costo'];
        $abierta = $fila['abierta'];
        $usuario = $fila['usuario'];
        $marca_vehiculo = $fila['marca_vehiculo'];
        $modelo_vehiculo = $fila['modelo_vehiculo'];
        $placa_vehiculo = $fila['placa_vehiculo'];        
        $recorrido_vehiculo = $fila['recorrido'];        
        $fecha_abierta = $fila['fecha_a'];
        $fecha_cierre = $fila['fecha_c'];

        if($abierta == 0)
        {
            $abierta = null;
        }
        
        $datos[] = array('id_ot'=>$id_ot, 
                         'id_vehiculo'=>$id_vehiculo,
                         'id_cliente' =>$id_cliente,
                         'nombre_cliente' =>$nombrecliente,
                         'tipo_cliente' =>$tipo_cliente,
                         'identificacion_cliente' =>$identificacion_cliente,
                         'tipo_servicio' =>$tipo_servicio,
                         'area_afectada' =>$area_afectada,
                         'tipo_trabajo' =>$tipo_trabajo,
                         'costo' =>$costo,
                         'abierta' =>$abierta,
                         'usuario' =>$usuario,
                         'marca_vehiculo' =>$marca_vehiculo,
                         'modelo_vehiculo' =>$modelo_vehiculo,
                         'placa_vehiculo' =>$placa_vehiculo,
                         'recorrido_vehiculo' =>$recorrido_vehiculo,
                         'fecha_cierre'=>$fecha_cierre,
                         'fecha_cierre_abierta'=>$fecha_abierta
                        );
        
        $json = json_encode($datos);
    }
    echo $json;
    
}

