<?php
require "php/connect.php";

$sql = "select * from empresa limit 10";

if($consulta = $mysql->query($sql))
{
    while($fila = $consulta->fetch_assoc())
    {
        $nombre = $fila['nombre'];
        $logo = $fila['logo'];
        
        ?>
        	<div class="carousel-item centrado_vert" style="width: 500px">
                <img src="logos/<?php echo $logo; ?>" class="tooltipped" data-position="top" 
                data-delay="50" data-tooltip="<?php echo $nombre; ?>">
            </div>
        <?php        
    }
}
else
{
	echo "No se a podido Ejecutar la consulta: ".mysqli_error($mysql);
}
?>