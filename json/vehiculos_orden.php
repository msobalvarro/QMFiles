<?php
require "../php/connect.php";



@session_start();
$id_session = $_SESSION['id_usuario'];
$empresa = $_SESSION['id_empresa'];

$sql = "SELECT vehiculo.*, cliente.activo as cliente_activo, cliente.nombres as nombre_cliente, cliente.identificacion as identificacion_cliente from vehiculo, cliente where cliente.id_empresa = '$empresa' and vehiculo.activo = 1 and cliente.activo = 1 and cliente.id = vehiculo.id_cliente ORDER BY marca ASC";

$datos = array();
if($consulta = $mysql->query($sql))
{
    while($fila = $consulta->fetch_assoc())
    {
        $id_vehiculo = $fila['id'];
        $id_cliente = $fila['id_cliente'];
        $nombre_cliente = $fila['nombre_cliente'];
        $identificacion_cliente = $fila['identificacion_cliente'];
        $marca = $fila['marca'];
        $modelo = $fila['modelo'];
        $color = $fila['color'];
        $placa = $fila['placa'];
        $motor = $fila['motor'];
        $chasis = $fila['chasis'];
        $recorrido = $fila['recorrido'];
        $cilindros = $fila['cilindros'];
        $transmicion = $fila['transmision'];
        $tipo_motor = $fila['tipo_motor'];
        $uso = $fila['uso'];
        
        $datos[] = array('id_vehiculo'=>$id_vehiculo, 'id_cliente'=>$id_cliente,'marca'=>$marca,'modelo'=>$modelo,'color'=>$color,'placa'=>$placa,'motor'=>$motor,'chasis'=>$chasis,'recorrido'=>$recorrido,'cilindros'=>$cilindros,'transmicion'=>$transmicion,'tipo_motor'=>$tipo_motor,'uso'=>$uso, 'nombre_cliente'=>$nombre_cliente, 'identificacion_cliente'=>$identificacion_cliente);
    }
    $json = json_encode($datos);
}
echo $json;