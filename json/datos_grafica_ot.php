<?php
require "../php/connect.php";


@session_start();
$empresa = $_SESSION['id_empresa'];

$sql = "
    
select count(*) as cantidad, 'Reacondicionamiento' as nombre from ot where tipo_servicio = 'Reacondicionamiento' and empresa = '$empresa'
union
select count(*) as cantidad, 'Diagnóstico' as nombre from ot where tipo_servicio = 'Diagnóstico' and empresa = '$empresa'
union
select count(*) as cantidad, 'Otro Servicio' as nombre from ot where tipo_servicio = 'Otro Servicio' and empresa = '$empresa'
union
select count(*) as cantidad, 'Mantenimiento Menor' as nombre from ot where tipo_servicio = 'Mantenimiento Menor' and empresa = '$empresa'
union
select count(*) as cantidad, 'Mantenimiento Mayor' as nombre from ot where tipo_servicio = 'Mantenimiento Mayor' and empresa = '$empresa'
union
select count(*) as cantidad, 'Reparación Menor' as nombre from ot where tipo_servicio = 'Reparación Menor' and empresa = '$empresa'
union
select count(*) as cantidad, 'Reparación Mayor' as nombre from ot where tipo_servicio = 'Reparación Mayor' and empresa = '$empresa'

";

$datos = array();
if($consulta = $mysql->query($sql))
{
    while($fila = $consulta->fetch_assoc())
    {
        $cantidad = $fila['cantidad'];
        $nombre = $fila['nombre'] .' ('. $fila['cantidad'].')';

        if($cantidad == 0)
        {
        	$cantidad = null;
        }
        
        $datos[] = array('text'=>$nombre, 'values'=>[$cantidad]);
    }
    $json = json_encode($datos, JSON_NUMERIC_CHECK);
}
else
{
	echo "Error: ".mysqli_error($mysql);
}
echo $json;
