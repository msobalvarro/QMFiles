<?php
require "../php/connect.php";


@session_start();
$empresa = $_SESSION['id_empresa'];

$sql = "select * from inventario where empresa='$empresa' and activo = 1 and existencia>0 order by nombre asc; ";

$datos = array();
if($consulta = $mysql->query($sql))
{
    while($fila = $consulta->fetch_assoc())
    {
        $id = $fila['id'];
        $nombre = $fila['nombre'];
        $codigo = $fila['codigo'];
        $precio = $fila['precio'];
        $existencia = $fila['existencia'];
        $descripcion = $fila['descripcion'];
        
        $datos[] = array('id'=>$id, 'nombre'=>$nombre, 'codigo'=>$codigo, 'descripcion'=>$descripcion, 'precio'=>$precio, 'existencia'=>$existencia, 'activo'=>true);
    }
    $json = json_encode($datos);
}
else{
    echo mysqli_error($mysql);
}
echo $json;
